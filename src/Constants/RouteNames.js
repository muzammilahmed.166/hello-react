const RouteList = {
    LOGIN: '/',
    RESET: '/reset',
    CHANGEPASSWORD:'/resetpassword/token=:token',
    DASHBOARD_LIVE: '/dashboard/live',
    DASHBOARD_SCHEDULE: '/dashboard/schedule',
    DASHBOARD_ASSIGNED: '/dashboard/Assigned',
    SCHEDULE: '/dashboard/schedule',
    CALLHISTORY: '/dashboard/call-history',
}

const RouteNames = Object.freeze(RouteList);

export default RouteNames;