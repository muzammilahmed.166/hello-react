import React, { Suspense } from 'react';
import { Toaster } from 'react-hot-toast';
import { useSelector } from 'react-redux';
import { Outlet, Route, Routes } from 'react-router-dom';
import DashboardLayout from './Layout/DashboardLayout';
import LoginLayout from './Layout/LoginLayout';
import Live from './Pages/Dashboard/CustomerQueue/Live';
import Schedule from './Pages/Dashboard/CustomerQueue/Schedule';
import Assigned from './Pages/Dashboard/CustomerQueue/Assigned';
import Dashboard from './Pages/Dashboard/Dashboard';
import Login from './Pages/Forms/Login';
import Reset from './Pages/Forms/Reset';
import ChangePassword from './Pages/Forms/ChangePassword';
import AadharCheck from './Pages/VideoDashboard/AadharCheck';
import CaptureFace from './Pages/VideoDashboard/CaptureFace';
import CheckLocation from './Pages/VideoDashboard/CheckLocation';
import Instructions from './Pages/VideoDashboard/Instructions';
import PAN from './Pages/VideoDashboard/PAN';
import Questions from './Pages/VideoDashboard/Questions';
import VideoDashboard from './Pages/VideoDashboard/VideoDashboard';
import AadharOfflineCheck from './Pages/VideoDashboard/AadharOfflineCheck';
import KYCReport from './Pages/VideoDashboard/KYCReport';
import CallHistory from './Pages/Dashboard/CallHistory/CallHistory';
import AadharDetailsCheck from './Pages/VideoDashboard/AadharDetailsCheck';
import PANDetailsCheck from './Pages/VideoDashboard/PANDetailsCheck';

// const Layout = React.lazy(() => import("./Layout/Layout"));
const PageNotFound = React.lazy(() => import("./Pages/PageNotFound/PageNotFound"));

function App() {

  const vcipDetails = useSelector(state => state.VcipReducer.vcipDetails)

  return (
    <>
      <Toaster />
      <Suspense fallback={<div>Loading...</div>}>
        <Routes>
          <Route path='/' element={<LoginLayout />}>
            <Route index element={<Login />} />
            <Route path='reset' element={<Reset />} />
            <Route path='/resetpassword/token=:token' element={<ChangePassword />} />
          </Route>
          <Route path='/dashboard' element={<DashboardLayout />}>
            <Route element={<Dashboard />}>
              <Route path='live' index element={<Live />} />
              <Route path='schedule' index element={<Schedule />} />
              <Route path='assigned' index element={<Assigned />} />

            </Route>
            <Route path='call-history' element={<CallHistory />} />
          </Route>
          <Route path='/video/:id' element={<VideoDashboard />}>
            <Route index element={<Instructions />} />
            <Route path='questions' element={<Questions />} />
            <Route path='location-check' element={<CheckLocation />} />
            <Route path='capture-face' element={<CaptureFace />} />
            <Route path='aadhar-check' element={<AadharCheck />} />
            <Route path='aadhar-details-check' element={<AadharDetailsCheck />} />
            {/* <Route path='aadhar-offline-check' element={<AadharOfflineCheck />} /> */}
            <Route path='pan' element={<PAN />} />
            <Route path='pan-details-check' element={<PANDetailsCheck />} />
            <Route path='KYC-Report' element={<KYCReport />} />
          </Route>
          <Route path='*' element={<PageNotFound />} />
        </Routes>
        <Outlet />
      </Suspense>
    </>
  );
}

export default App;
