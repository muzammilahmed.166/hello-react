import React from 'react';
import RouteNames from '../Constants/RouteNames';

const Login = React.lazy(() => import("../Pages/Forms/Login"));
const Reset = React.lazy(() => import("../Pages/Forms/Reset"));
const ChangePassword = React.lazy(()=> import("../Pages/Forms/ChangePassword"))

const routes = [
    {
        path: RouteNames.LOGIN, element: <Login />
    },
    {
        path: RouteNames.RESET, element: <Reset />
    },
    {
        path: RouteNames.CHANGEPASSWORD, element: <ChangePassword />
    },
]

export default routes;