import React from 'react'

const InitiatingVideoCallCmp = () => {
  return (
    <section className="app-start">
      <div className="text-center mb-3">
        <img src="../../images/waiting.svg" className='spin-loader' alt="waiting" />
      </div>
      <h4 className="title text-center">Please wait...</h4>
      <p className="txt text-center">
        While we are searching for the customer to
        initiate the video call
      </p>
    </section>
  )
}

export default InitiatingVideoCallCmp