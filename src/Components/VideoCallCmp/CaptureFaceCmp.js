import React from 'react'

const CaptureFaceCmp = (props) => {

    return (
        <>
            <article className='prt-1'>
                <h5>Capture face</h5>
                <hr className="hr-horizontal" />
                <div className="check-location">
                    <div className="face-circle">
                        {props?.isCaptured
                            ? <img className="img-cirlce mt-5" src={props?.img} alt="" />
                            :
                            <div className="img-circle mt-5"> <img className="img-cirlce" src="../../images/circle.svg" alt="" />
                                {/* <img src="../../images/noimg.png " className="no-imgcaptured" /> */}
                            </div>
                        }
                        <div className="w-100 d-flex cap-btn" >
                            {props?.isCaptured ?
                                <>
                                    {/* <div style ={{marginTop:"150%", marginLeft:"-2%", display:"flex"}}> */}
                                    {/* {!props?.isConfirmed &&  */}
                                    <button className="retake-btn btn-white m-auto mt-3 " onClick={props?.retake}>
                                        Retake
                                    </button>
                                    {/* } */}
                                    {!props?.isConfirmed
                                        && <button className="cus-blue-btn m-auto mt-3" onClick={props?.confirm}>
                                            Confirm
                                        </button>
                                    }
                                    {/* </div> */}
                                </>
                                : <button className="cus-blue-btn m-auto mt-3" onClick={props?.capture}>
                                    <img src="../../images/Vector-camera.svg" alt="" />
                                    Capture
                                </button>
                            }
                        </div>
                    </div>
                    {props?.isConfirmed && <div className="match-score mt-4">
                        <h5>Face Liveness score - </h5>
                        {props?.livenessData?.confidence ? parseInt(props?.livenessData?.confidence) > '50' ? <span className="text-success faceliveness-score">{props?.livenessData?.confidence}%</span> : <span className="text-danger faceliveness-score">{props?.livenessData?.confidence}%</span> : '-'}
                    </div>}
                </div>
            </article>
            <article className='prt-2'>
                <button
                    className="btn"
                    disabled={!props?.isConfirmed}
                    onClick={props?.isConfirmed ? props?.nextPage : null}>
                    Next
                </button>
            </article>
        </>
    )
}

export default CaptureFaceCmp;