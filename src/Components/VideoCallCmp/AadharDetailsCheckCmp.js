import React from 'react'

const AadharDetailsCheckCmp = (props) => {
    return (
        <>
            <article className='prt-1'>
                <h5>Check Aadhaar Details</h5>
                <hr className="hr-horizontal" />
                <div style={{ overflowX: 'auto' }} className="table-customer-details">
                    <table className="table kycdetails table-borderless">
                        <thead>
                            <tr className='vertical-align-middle'>
                                <th className="kyc-header">User details</th>
                                <th className="kyc-header">Applicant from data</th>
                                <th className="kyc-header">Aadhaar data</th>
                                <th className="kyc-header">Match</th>
                            </tr>
                        </thead>
                        <tbody className="table-userdetails">
                            <tr>
                               <td className ="kycdetails-td">Name</td>
                               <td className ="kycdetails-td">{props?.customerVcipDetails?.customerdetails?.name ? props?.customerVcipDetails?.customerdetails?.name : "-"}</td>
                               <td className ="kycdetails-td">{props?.customerVcipDetails?.kycdetails?.name ? props?.customerVcipDetails?.kycdetails?.name : "-"}</td>
                               <td className ="kycdetails-td">{props?.customerVcipDetails?.fuzzymatchdetails?.kyc_name ? props?.customerVcipDetails?.fuzzymatchdetails?.kyc_name > '50%'? <span className ="text-success">{props?.customerVcipDetails?.fuzzymatchdetails?.kyc_name}</span>:<span className ="text-danger">{props?.customerVcipDetails?.fuzzymatchdetails?.kyc_name}</span> : "-"}</td>
                           </tr>
                           <tr>
                               <td className ="kycdetails-td">Father’s name</td>
                               <td className ="kycdetails-td">{props?.customerVcipDetails?.customerdetails?.fname ? props?.customerVcipDetails?.customerdetails?.fname : "-"}</td>
                               <td className ="kycdetails-td">{props?.customerVcipDetails?.kycdetails?.fname ? props?.customerVcipDetails?.kycdetails?.fname : "-"}</td>
                               <td className ="kycdetails-td">{props?.customerVcipDetails?.fuzzymatchdetails?.kyc_name ? props?.customerVcipDetails?.fuzzymatchdetails?.kyc_name > '50%'? <span className ="text-success">{props?.customerVcipDetails?.fuzzymatchdetails?.kyc_name}</span>:<span className ="text-danger">{props?.customerVcipDetails?.fuzzymatchdetails?.kyc_name}</span> : "-"}</td>
                           </tr>
                           <tr>
                               <td className ="kycdetails-td">DOB</td>
                               <td className ="kycdetails-td">{props?.customerVcipDetails?.customerdetails?.dob ? props?.customerVcipDetails?.customerdetails?.dob : "-"}</td>
                               <td className ="kycdetails-td">{props?.customerVcipDetails?.kycdetails?.dob ? props?.customerVcipDetails?.kycdetails?.dob : "-"}</td>
                               <td className ="kycdetails-td">{props?.customerVcipDetails?.fuzzymatchdetails?.kyc_dob ? props?.customerVcipDetails?.fuzzymatchdetails?.kyc_dob >'50%' ? <span className ="text-success">{props?.customerVcipDetails?.fuzzymatchdetails?.kyc_dob}</span>:<span className ="text-danger">{props?.customerVcipDetails?.fuzzymatchdetails?.kyc_dob}</span> : "-"}</td>
                           </tr>

                           <tr>
                               <td className ="kycdetails-td">Gender</td>
                               <td className ="kycdetails-td">{props?.customerVcipDetails?.customerdetails?.gender ? props?.customerVcipDetails?.customerdetails?.gender : "-"}</td>
                               <td className ="kycdetails-td">{props?.customerVcipDetails?.kycdetails?.gender ? props?.customerVcipDetails?.kycdetails?.gender : "-"}</td>
                               <td className ="kycdetails-td">{props?.customerVcipDetails?.fuzzymatchdetails?.kyc_gender ? parseInt(props?.customerVcipDetails?.fuzzymatchdetails?.kyc_gender) >parseInt('50%')? <span className ="text-success">{props?.customerVcipDetails?.fuzzymatchdetails?.kyc_gender}</span>:<span className ="text-danger">{props?.customerVcipDetails?.fuzzymatchdetails?.kyc_gender}</span> : "-"}</td>
                           </tr>
                           <tr>
                               <td className ="kycdetails-td">Current address</td>
                               <td className ="kycdetails-td">{props?.customerVcipDetails?.customerdetails?.curr_address ? props?.customerVcipDetails?.customerdetails?.curr_address : "-"}</td>
                               <td className ="kycdetails-td">{props?.customerVcipDetails?.kycdetails?.address ? props?.customerVcipDetails?.kycdetails?.address : "-"}</td>
                               <td className ="kycdetails-td">{props?.customerVcipDetails?.fuzzymatchdetails?.kyc_curr_address ? props?.customerVcipDetails?.fuzzymatchdetails?.kyc_curr_address >'50%'? <span className ="text-success">{props?.customerVcipDetails?.fuzzymatchdetails?.kyc_curr_address}</span>:<span className ="text-danger">{props?.customerVcipDetails?.fuzzymatchdetails?.kyc_curr_address}</span> : "-"}</td>
                           </tr>
                           <tr>
                               <td className ="kycdetails-td">Permanent address</td>
                               <td className ="kycdetails-td"> {props?.customerVcipDetails?.customerdetails?.curr_address ? props?.customerVcipDetails?.customerdetails?.curr_address : "-"}</td>
                               <td className ="kycdetails-td">{props?.customerVcipDetails?.kycdetails?.address ? props?.customerVcipDetails?.kycdetails?.address : "-"}</td>
                               <td className ="kycdetails-td">{props?.customerVcipDetails?.fuzzymatchdetails?.kyc_per_address ? props?.customerVcipDetails?.fuzzymatchdetails?.kyc_per_address >'50%'? <span className ="text-success">{props?.customerVcipDetails?.fuzzymatchdetails?.kyc_per_address}</span>:<span className ="text-danger">{props?.customerVcipDetails?.fuzzymatchdetails?.kyc_per_address}</span> : "-"}</td>
                           </tr>
                           <tr>
                               <td className ="kycdetails-td">Mobile number</td>
                               <td className ="kycdetails-td">{props?.customerVcipDetails?.customerdetails?.mobile ? props?.customerVcipDetails?.customerdetails?.mobile : "-"}</td>
                               <td className ="kycdetails-td">-</td>
                               <td className ="kycdetails-td">-</td>
                           </tr>
                           <tr>
                               <td className ="kycdetails-td">Email address</td>
                               <td className ="kycdetails-td">{props?.customerVcipDetails?.customerdetails?.email ? props?.customerVcipDetails?.customerdetails?.email : "-"}</td>
                               <td className ="kycdetails-td">-</td>
                               <td className ="kycdetails-td">-</td>
                           </tr>
                            {/* <tr>
                                <td className="kyc-reportdataheader">
                                    <p>Name</p>
                                    <p>Father’s name</p>
                                    <p>DOB</p>
                                    <p>Gender</p>
                                    <p>Current
                                        address</p>
                                    <p>Permanent address</p>
                                    <p>Mobile number</p>
                                    <p>Email address</p>
                                </td>
                                <td className="kyc-reportdata">
                                    <p className="kyc-data">{props?.customerVcipDetails?.customerdetails?.name ? props?.customerVcipDetails?.customerdetails?.name : "-"}</p>
                                    <p className="kyc-data">{props?.customerVcipDetails?.customerdetails?.fname ? props?.customerVcipDetails?.customerdetails?.fname : "-"}</p>
                                    <p className="kyc-data">{props?.customerVcipDetails?.customerdetails?.dob ? props?.customerVcipDetails?.customerdetails?.dob : "-"}</p>
                                    <p className="kyc-data">{props?.customerVcipDetails?.customerdetails?.gender ? props?.customerVcipDetails?.customerdetails?.gender : "-"}</p>
                                    <p className="kyc-data">{props?.customerVcipDetails?.customerdetails?.curr_address ? props?.customerVcipDetails?.customerdetails?.curr_address : "-"}</p>
                                    <p className="kyc-data">{props?.customerVcipDetails?.customerdetails?.curr_address ? props?.customerVcipDetails?.customerdetails?.curr_address : "-"}</p>
                                    <p className="kyc-data">{props?.customerVcipDetails?.customerdetails?.mobile ? props?.customerVcipDetails?.customerdetails?.mobile : "-"}</p>
                                    <p className="kyc-data">{props?.customerVcipDetails?.customerdetails?.email ? props?.customerVcipDetails?.customerdetails?.email : "-"}</p>
                                </td>
                                <td className="kyc-reportdata">
                                    <p className="kyc-data">{props?.customerVcipDetails?.kycdetails?.name ? props?.customerVcipDetails?.kycdetails?.name : "-"}</p>
                                    <p className="kyc-data">{props?.customerVcipDetails?.kycdetails?.fname ? props?.customerVcipDetails?.kycdetails?.fname : "-"}</p>
                                    <p className="kyc-data">{props?.customerVcipDetails?.kycdetails?.dob ? props?.customerVcipDetails?.kycdetails?.dob : "-"}</p>
                                    <p className="kyc-data">{props?.customerVcipDetails?.kycdetails?.gender ? props?.customerVcipDetails?.kycdetails?.gender : "-"}</p>
                                    <p className="kyc-data">{props?.customerVcipDetails?.kycdetails?.address ? props?.customerVcipDetails?.kycdetails?.address : "-"}</p>
                                    <p className="kyc-data">{props?.customerVcipDetails?.kycdetails?.address ? props?.customerVcipDetails?.kycdetails?.address : "-"}</p>
                                    <p className="kyc-data">-</p>
                                    <p className="kyc-data">-</p>
                                </td>
                                <td className="kyc-reportdata">
                                    <p className="kyc-match ">{props?.customerVcipDetails?.fuzzymatchdetails?.kyc_name ? props?.customerVcipDetails?.fuzzymatchdetails?.kyc_name > '50%'? <span className ="text-success">{props?.customerVcipDetails?.fuzzymatchdetails?.kyc_name}</span>:<span className ="text-danger">{props?.customerVcipDetails?.fuzzymatchdetails?.kyc_name}</span> : "-"}</p>
                                    <p className="kyc-match"> {props?.customerVcipDetails?.fuzzymatchdetails?.kyc_fname ? props?.customerVcipDetails?.fuzzymatchdetails?.kyc_fname > '50%' ? <span className ="text-success">{props?.customerVcipDetails?.fuzzymatchdetails?.kyc_fname }</span>:<span className ="text-danger">{props?.customerVcipDetails?.fuzzymatchdetails?.kyc_fname }</span> : "-"}</p>
                                    <p className="kyc-match">{props?.customerVcipDetails?.fuzzymatchdetails?.kyc_dob ? props?.customerVcipDetails?.fuzzymatchdetails?.kyc_dob >'50%' ? <span className ="text-success">{props?.customerVcipDetails?.fuzzymatchdetails?.kyc_dob}</span>:<span className ="text-danger">{props?.customerVcipDetails?.fuzzymatchdetails?.kyc_dob}</span> : "-"}</p>
                                    <p className="kyc-match text-success">{props?.customerVcipDetails?.fuzzymatchdetails?.kyc_gender ? parseInt(props?.customerVcipDetails?.fuzzymatchdetails?.kyc_gender) >parseInt('50%')? <span className ="text-success">{props?.customerVcipDetails?.fuzzymatchdetails?.kyc_gender}</span>:<span className ="text-danger">{props?.customerVcipDetails?.fuzzymatchdetails?.kyc_gender}</span> : "-"}</p>
                                    <p className="kyc-match text-warning">{props?.customerVcipDetails?.fuzzymatchdetails?.kyc_curr_address ? props?.customerVcipDetails?.fuzzymatchdetails?.kyc_curr_address >'50%'? <span className ="text-success">{props?.customerVcipDetails?.fuzzymatchdetails?.kyc_curr_address}</span>:<span className ="text-danger">{props?.customerVcipDetails?.fuzzymatchdetails?.kyc_curr_address}</span> : "-"}</p>
                                    <p className="kyc-match text-success">{props?.customerVcipDetails?.fuzzymatchdetails?.kyc_per_address ? props?.customerVcipDetails?.fuzzymatchdetails?.kyc_per_address >'50%'? <span className ="text-success">{props?.customerVcipDetails?.fuzzymatchdetails?.kyc_per_address}</span>:<span className ="text-danger">{props?.customerVcipDetails?.fuzzymatchdetails?.kyc_per_address}</span> : "-"}</p>
                                    <p className="kyc-match text-success">-</p>
                                    <p className="kyc-match text-success">-</p>
                                </td>
                            </tr> */}
                        </tbody>
                    </table>
                    <div className="pan-status-result">
                        <h4>XML Generation date:</h4>
                        <span>-</span>
                        <h5> {props?.customerVcipDetails?.kycdetails?.offlinekycgendate}</h5>
                        <img src="../../images/Vector-solid-right.svg" alt="" />
                    </div>
                </div>
            </article>
            <article className='prt-2'>
                <button className="btn" onClick={props?.nextPage} >Next {props.isLoading ? <span className ="spinner"></span> :null}</button>
            </article>
        </>
    )
}

export default AadharDetailsCheckCmp;