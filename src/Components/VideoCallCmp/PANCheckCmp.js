import React from 'react'

const PANCheckCmp = (props) => {
    return (
        <>
            <article className='prt-1'>
                <h5>Capture PAN</h5>
                <hr className="hr-horizontal" />
                <div className="row">
                    <div className="col-md-6">
                        <div className="aadhaar-offline-kyc">
                            <img
                                src={props?.customerVcipDetails?.pandetails?.aipht
                                    ? "data:image/png;base64," + props?.customerVcipDetails?.pandetails?.aipht
                                    : ''} alt="" />
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="aadhaar-offline-kyc">
                            <img src={props?.img ? props?.img : ''} alt="" />
                        </div>
                    </div>
                </div>
                <div className='row justify-content-center'>
                    <div className="col-md-6">
                        <div className="aadhaar-offline-kyc bg-white mt-3">
                            {/* <img src={props?.img ? props?.img : ''} alt="" /> */}
                            <div className="pan-bx-info">
                                <h6 className='vd-bx-info-ttl'>PAN details</h6>
                                <hr />

                                <table className="table">
                                    <tbody>
                                        <tr>
                                            <td>Name</td>
                                            <td>{props?.customerVcipDetails?.pandetails?.ainame}</td>
                                        </tr>
                                        <tr>
                                            <td>Father Name</td>
                                            <td>{props?.customerVcipDetails?.pandetails?.aifname}</td>
                                        </tr>
                                        <tr>
                                            <td>Date of birth</td>
                                            <td>{props?.customerVcipDetails?.pandetails?.aidob}</td>
                                        </tr>
                                        <tr>
                                            <td>PAN No</td>
                                            <td>{props?.customerVcipDetails?.pandetails?.aipannumber}</td>
                                        </tr>
                                    </tbody>
                                </table>
                                {/* <ul className="vd-bx-info-lst">
                                    <li><span>Name:</span> {props?.customerVcipDetails?.pandetails?.ainame}</li>
                                    <li><span>Father name:</span> {props?.customerVcipDetails?.pandetails?.aifname} </li>
                                    <li><span>Date of birth:</span> {props?.customerVcipDetails?.pandetails?.aidob}</li>
                                    <li><span>PAN No:</span> {props?.customerVcipDetails?.pandetails?.aipannumber}</li>
                                </ul> */}
                            </div>
                        </div>
                    </div>
                </div>
                <div className="match-score mt-4">
                    <h5>Face Match score - </h5>
                    {props?.facematchDetails?.confidence ? props?.facematchDetails?.confidence > '50'? <span className ="text-success faceliveness-score">{props?.facematchDetails?.confidence}%</span>:<span className ="text-danger faceliveness-score">{props?.facematchDetails?.confidence}%</span> : '-'}
                </div>
                <div className="match-score">
                    <h5>Does the face match with PAN photo?</h5>
                    {!props?.isMatchChecked && <>
                        <button className="btn-round" onClick={() => props?.updateFacematchStatus("0")}>
                            <img src='../../images/icon-wrong.svg' alt="check" />
                        </button>
                        <button className="btn-round btn-green" onClick={() => props?.updateFacematchStatus("1")}>
                            <img src='../../images/icon-check.svg' alt="check" />
                        </button>
                    </>}
                    {props?.isMatchChecked ? (<button className={`btn-round ${props?.isSelected === '0' ? '' : 'btn-green'}`}>
                        <img src={`${props?.isSelected === '0' ? '../../images/icon-wrong.svg' : '../../images/icon-check.svg'}`} alt="check" />
                    </button>)
                        : (null)
                    }
                </div>

                {/* {props?.isMatchChecked &&
                    <div style={{ overflowX: 'auto' }} className="table-customer-details">
                        <table className="table kycdetails">
                            <thead>
                                <tr className='vertical-align-middle'>
                                    <th className="kyc-header vertical-align-middle">User details</th>
                                    <th className="kyc-header ">Applicant from data</th>
                                    <th className="kyc-header">OCR output</th>
                                    <th className="kyc-header">Match</th>
                                </tr>
                            </thead>
                            <tbody className="table-userdetails">
                                <tr>
                                    <td className="kyc-reportdataheader">
                                        <p>Name</p>
                                        <p>Father’s name</p>
                                        <p>DOB</p>
                                        <p>PAN Number</p>
                                    </td>
                                    <td className="kyc-reportdata">
                                        <p className="kyc-data">{props?.customerVcipDetails?.pandetails?.ainame ? props?.customerVcipDetails?.pandetails?.ainame : "-"}</p>
                                        <p className="kyc-data">{props?.customerVcipDetails?.pandetails?.aifname ? props?.customerVcipDetails?.pandetails?.aifname : "-"}</p>
                                        <p className="kyc-data">{props?.customerVcipDetails?.pandetails?.aidob ? props?.customerVcipDetails?.pandetails?.aidob : "-"}</p>
                                        <p className="kyc-data">{props?.customerVcipDetails?.pandetails?.aipannumber ? props?.customerVcipDetails?.pandetails?.aipannumber : "-"}</p>
                                    </td>
                                    <td className="kyc-reportdata">
                                        <p className="kyc-data">{props?.customerVcipDetails?.pandetails?.oemname ? props?.customerVcipDetails?.pandetails?.oemname : "-"}</p>
                                        <p className="kyc-data">{props?.customerVcipDetails?.pandetails?.oemfname ? props?.customerVcipDetails?.pandetails?.oemfname : "-"}</p>
                                        <p className="kyc-data">{props?.customerVcipDetails?.pandetails?.oemdob ? props?.customerVcipDetails?.pandetails?.oemdob : "-"}</p>
                                        <p className="kyc-data">{props?.customerVcipDetails?.pandetails?.oemstatus ? props?.customerVcipDetails?.pandetails?.oemstatus : "-"}</p>
                                    </td>
                                    <td className="kyc-reportdata">
                                        <p className="kyc-match text-success">{props?.customerVcipDetails?.fuzzymatchdetails?.kyc_pan_name ? props?.customerVcipDetails?.fuzzymatchdetails?.kyc_pan_name : "-"}</p>
                                        <p className="kyc-match text-danger"> {props?.customerVcipDetails?.fuzzymatchdetails?.kyc_pan_fname ? props?.customerVcipDetails?.fuzzymatchdetails?.kyc_pan_fname : "-"}</p>
                                        <p className="kyc-match text-success">{props?.customerVcipDetails?.fuzzymatchdetails?.kyc_pan_dob ? props?.customerVcipDetails?.fuzzymatchdetails?.kyc_pan_dob : "-"}</p>
                                        <p className="kyc-match text-success">{props?.customerVcipDetails?.fuzzymatchdetails?.pan_pannumber ? props?.customerVcipDetails?.fuzzymatchdetails?.pan_pannumber : "-"}</p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div className="pan-status-result">
                            <h4>XML Generation date:</h4>
                            <span>-</span>
                            <h5> {props?.customerVcipDetails?.kycdetails?.offlinekycgendate}</h5>
                            <img src="../../images/Vector-solid-right.svg" alt="" />
                        </div>
                    </div>
                } */}

            </article>
            <article className='prt-2'>
                <button className="btn" onClick={props?.nextPage}>Next</button>
                {/* <button className="btn" onClick={props?.nextPage}>Next {props.isLoading ? <span className ="spinner"></span>:null}</button> */}
            </article>
        </>
    )
}

export default PANCheckCmp;