import React from 'react'

const AadhaarDigiLockerCmp = (props) => {
    return (
        <section className="app-start">
            {!props?.isAadharSuccess
                ? <>
                    <div className="text-center mb-3">
                        <img src="../images/waiting.svg" className='spin-loader' alt="waiting" />
                    </div>
                    <h4 className="title text-center">Please wait...</h4>
                    <p className="txt text-center">
                        Fetching Aadhar Details...!
                    </p>
                </>
                :
                <div className="aadhar-details">
                    <h4 className="title mb-2">Aadhar Details</h4>
                    <div className='ocr-pht'>
                        <img src={"data:image/png;base64," + props?.aadharDetails?.pht} alt='photo' />
                    </div>
                    <div className="form-floating mb-3">
                        <input
                            type="text"
                            className="form-control"
                            defaultValue={props?.aadharDetails?.name}
                            id="floatingInput"
                            readOnly
                        />
                        <label htmlFor="floatingInput">Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input
                            type="text"
                            className="form-control"
                            defaultValue={props?.aadharDetails?.fname}
                            id="floatingInput"
                            readOnly
                        />
                        <label htmlFor="floatingInput">Father Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input
                            type="text"
                            className="form-control"
                            defaultValue={props?.aadharDetails?.uid}
                            id="floatingInput"
                            readOnly
                        />
                        <label htmlFor="floatingInput">UID</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input
                            type="text"
                            className="form-control"
                            defaultValue={props?.aadharDetails?.dob}
                            id="floatingInput"
                            readOnly
                        />
                        <label htmlFor="floatingInput">DOB</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input
                            type="text"
                            className="form-control"
                            defaultValue={props?.aadharDetails?.gender}
                            id="floatingInput"
                            readOnly
                        />
                        <label htmlFor="floatingInput">Gender</label>
                    </div>
                    <div className="form-floating">
                        <textarea
                            className="form-control"
                            id="floatingTextarea"
                            defaultValue={props?.aadharDetails?.address}
                            readOnly
                        />
                        <label htmlFor="floatingTextarea">Address</label>
                    </div>

                </div>}
        </section>
    )
}

export default AadhaarDigiLockerCmp;