import React from 'react';
import { Link } from 'react-router-dom';
import RouteNames from '../../Constants/RouteNames';

const SelectKYCProcessCmp = () => {
    return (
        <section className="app-start">
            <div className="app-body-img">
                <img src="../images/icon-identity.svg" alt="vkyc" />
            </div>
            <div className="app-body-data">
                <h4 className="title">Verify your identity</h4>
                <p className="txt">
                    Please keep your following documents
                    handy before you proceed with your full
                    KYC process.
                </p>
            </div>
            <ul className="video-guide">
                <li>
                    {/* <Link to={RouteNames.AADHAR_KYC_PROCESS_UPLOAD} className="identity-names">Aadhaar Verification</Link> */}
                    <p className="identity-names">Aadhaar Verification</p>
                </li>
                <li>
                    <p className="identity-names">PAN capture</p>
                </li>
                <li>
                    <p className="identity-names">Video Call (Q&A)</p>
                </li>
                <li>
                    <p className="identity-names">Photo Capture</p>
                </li>
                <li>
                    <p className="identity-names">Signature Capture</p>
                </li>
            </ul>
        </section>
    )
}

export default SelectKYCProcessCmp