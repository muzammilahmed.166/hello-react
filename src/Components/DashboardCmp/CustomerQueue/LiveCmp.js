import React from 'react'
import StatusCodes from '../../../Constants/StatusCodes';
import { useTable, usePagination, useGlobalFilter, useSortBy } from "react-table";
import NoData from '../../../Pages/Dashboard/NoData';


const LiveCmp = (props) => {
    // Our table component
    function Table({ columns, data }) {
        const props = useTable(
            {
                columns,
                data
            },
            useGlobalFilter, // useGlobalFilter!
            useSortBy,
            usePagination,
        );
        const {
            getTableProps,
            getTableBodyProps,
            headerGroups,
            rows,
            prepareRow,
            setGlobalFilter,
            state,
            page, // Instead of using 'rows', we'll use page,
            // which has only the rows for the active page

            // The rest of these things are super handy, too ;)
            canPreviousPage,
            canNextPage,
            pageOptions,
            pageCount,
            gotoPage,
            nextPage,
            previousPage,
            setPageSize,
            state: { pageIndex, pageSize, globalFilter }
        } = props;
        React.useEffect(() => {
            // props.dispatch({ type: actions.resetPage })
        }, [globalFilter]);

        return (
            <>
                {data && data.length > 0
                    ? <input
                        type="text"
                        placeholder="Search"
                        value={globalFilter || ""}
                        onChange={e => setGlobalFilter(e.target.value)}
                    />
                    : (null)
                }
                <table {...getTableProps()}>
                    <thead>
                        {headerGroups.map(headerGroup => (
                            <tr {...headerGroup.getHeaderGroupProps()}>
                                {headerGroup.headers.map(column => (
                                    <th {...column.getHeaderProps(column.getSortByToggleProps())}>
                                        {column.render("Header")}
                                        {/* Render the columns filter UI */}
                                        <span>
                                            {column.isSorted
                                                ? column.isSortedDesc
                                                    ? ' 🔽'
                                                    : ' 🔼'
                                                : ''}
                                        </span>
                                    </th>
                                ))}
                            </tr>
                        ))}
                    </thead>
                    <tbody {...getTableBodyProps()}>
                        {page.map((row, i) => {
                            prepareRow(row);
                            return (
                                <tr {...row.getRowProps()}>
                                    {row.cells.map(cell => {
                                        return (
                                            <td {...cell.getCellProps()}>{cell.render("Cell")}</td>
                                        );
                                    })}
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
                {data && data.length > 0
                    ? <>
                        <div className="pagination">
                            <button onClick={() => gotoPage(0)} disabled={!canPreviousPage}>
                                {"<<"}
                            </button>{" "}
                            <button onClick={() => previousPage()} disabled={!canPreviousPage}>
                                {"<"}
                            </button>{" "}
                            <button onClick={() => nextPage()} disabled={!canNextPage}>
                                {">"}
                            </button>{" "}
                            <button onClick={() => gotoPage(pageCount - 1)} disabled={!canNextPage}>
                                {">>"}
                            </button>{" "}

                            <div className='pag-num'>
                                <span>
                                    Page{" "}
                                    <strong>
                                        {pageIndex + 1} of {pageOptions.length}
                                    </strong>{" "}
                                </span>
                                <span>
                                    | Go to page:{" "}
                                    <input
                                        type="number"
                                        defaultValue={pageIndex + 1}
                                        onChange={e => {
                                            const page = e.target.value ? Number(e.target.value) - 1 : 0;
                                            gotoPage(page);
                                        }}
                                        style={{ width: "100px" }}
                                    />
                                </span>{" "}
                                <select
                                    value={pageSize}
                                    onChange={e => {
                                        setPageSize(Number(e.target.value));
                                    }}
                                >
                                    {[10, 20, 30, 40, 50].map(pageSize => (
                                        <option key={pageSize} value={pageSize}>
                                            Show {pageSize}
                                        </option>
                                    ))}
                                </select>
                            </div>
                        </div>
                        <br />
                        <div>Showing the first 20 results of {rows.length} rows</div>
                        <div>
                            <pre>
                                <code>{JSON.stringify(state.filters, null, 2)}</code>
                            </pre>
                        </div>
                    </>
                    : null}
            </>
        );
    }

    const columns = React.useMemo(
        () => [
            {
                Header: "Name",
                columns: [
                    {
                        Header: "Customer Name",
                        accessor: "customer name",
                    },
                    {
                        Header: "App ID",
                        accessor: "app id",
                        filter: "equals"
                    },
                    {
                        Header: "Location",
                        accessor: "location",
                    },
                    {
                        Header: "IP Status",
                        accessor: "ip status",
                        filter: "includes"
                    },
                    {
                        Header: "Waiting Since",
                        accessor: "waiting since",
                    },
                    {
                        Header: "Status",
                        accessor: "status",
                    }
                ]
            }
        ],
        []
    );

    return (
        // <Table columns={columns} data={props?.liveList} />
        <>
            {props?.liveList && props?.liveList?.length > 0
                ? <Table columns={columns} data={props?.liveList} />
                :
                <>
                    <Table columns={columns} data={[]} />
                    <NoData
                        title="Sit back and relax!"
                        subtitle="There are currently no live customers in the queue"
                        img="../images/icon-sleepy.svg"
                        pageName="live"
                    />
                </>
            }
        </>

        // <table className="table">
        //     <thead>
        //         <tr>
        //             <th scope="col">Customer name</th>
        //             <th scope="col">App ID</th>
        //             <th scope="col">location</th>
        //             <th scope="col">IP Status</th>
        //             <th scope="col">Waiting since</th>
        //             <th scope="col">Status</th>
        //         </tr>
        //     </thead>
        //     <tbody>
        //         {props?.liveList?.length > 0
        //             ? props?.liveList?.map((item, i) => <tr key={i}>
        //                 <td>{item?.name}</td>
        //                 <td>{item?.appid}</td>
        //                 <td>{item?.customerloc}</td>
        //                 <td>{item?.customeripstatus}</td>
        //                 <td>{item?.wtime}</td>
        //                 <td>
        //                     {item?.joinstatus === StatusCodes.VCIP_JOINSTATUS_ENABLE
        //                         ? <button className='btn' onClick={() => props?.joinVideoSession(item)}>Join Now</button>
        //                         : <button className='btn' disabled>Join Now</button>}

        //                 </td>
        //             </tr>)
        //             : (<tr><td colSpan={5} className='text-center'>No VCIPID's Found.</td></tr>)}
        //     </tbody>
        // </table>


    )
}

export default LiveCmp;