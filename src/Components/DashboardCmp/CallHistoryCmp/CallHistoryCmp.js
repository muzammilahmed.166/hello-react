import React from 'react';
import { useTable, usePagination, useGlobalFilter, useSortBy } from "react-table";
import NoData from '../../../Pages/Dashboard/NoData';

const CallHistoryCmp = (props) => {

    function Table({ columns, data }) {
        const props = useTable(
            {
                columns,
                data
            },
            useGlobalFilter, // useGlobalFilter!
            useSortBy,
            usePagination,
        );
        const {
            getTableProps,
            getTableBodyProps,
            headerGroups,
            rows,
            prepareRow,
            setGlobalFilter,
            state,
            page, // Instead of using 'rows', we'll use page,
            // which has only the rows for the active page

            // The rest of these things are super handy, too ;)
            canPreviousPage,
            canNextPage,
            pageOptions,
            pageCount,
            gotoPage,
            nextPage,
            previousPage,
            setPageSize,
            state: { pageIndex, pageSize, globalFilter }
        } = props;
        React.useEffect(() => {
            // props.dispatch({ type: actions.resetPage })
        }, [globalFilter]);

        return (
            <>
                <div className="tab-pane fade show active" id="queue" role="tabpanel" aria-labelledby="queue-tab">
                    {data && data?.length > 0
                        ? <div className='d-flex align-items-center mb-5 '>
                            <input
                                type="text"
                                placeholder="Search"
                                value={globalFilter || ""}
                                onChange={e => setGlobalFilter(e.target.value)}
                            />
                        </div>
                        : null}

                    <table className="mt-5" {...getTableProps()}>
                        <thead>
                            {headerGroups.map(headerGroup => (
                                <tr {...headerGroup.getHeaderGroupProps()}>
                                    {headerGroup.headers.map(column => (
                                        <th {...column.getHeaderProps(column.getSortByToggleProps())}>
                                            {column.render("Header")}
                                            {/* Render the columns filter UI */}
                                            <span>
                                                {column.isSorted
                                                    ? column.isSortedDesc
                                                        ? ' 🔽'
                                                        : ' 🔼'
                                                    : ''}
                                            </span>
                                        </th>
                                    ))}
                                </tr>
                            ))}
                        </thead>
                        <tbody {...getTableBodyProps()}>
                            {page.map((row, i) => {
                                prepareRow(row);
                                return (

                                    <tr {...row.getRowProps()}>
                                        {row.cells.map(cell => {
                                            return (
                                                <td {...cell.getCellProps()}>{cell.render("Cell")}</td>
                                            );
                                        })}
                                    </tr>

                                );
                            })}
                        </tbody>
                    </table>

                    <div className="pagination">
                        <button onClick={() => gotoPage(0)} disabled={!canPreviousPage}>
                            {"<<"}
                        </button>{" "}
                        <button onClick={() => previousPage()} disabled={!canPreviousPage}>
                            {"<"}
                        </button>{" "}
                        <button onClick={() => nextPage()} disabled={!canNextPage}>
                            {">"}
                        </button>{" "}
                        <button onClick={() => gotoPage(pageCount - 1)} disabled={!canNextPage}>
                            {">>"}
                        </button>{" "}

                        <div className='pag-num'>


                            <span>
                                Page{" "}
                                <strong>
                                    {pageIndex + 1} of {pageOptions.length}
                                </strong>{" "}
                            </span>
                            <span>
                                | Go to page:{" "}
                                <input
                                    type="number"
                                    defaultValue={pageIndex + 1}
                                    onChange={e => {
                                        const page = e.target.value ? Number(e.target.value) - 1 : 0;
                                        gotoPage(page);
                                    }}
                                    style={{ width: "100px" }}
                                />
                            </span>{" "}
                            <select
                                value={pageSize}
                                onChange={e => {
                                    setPageSize(Number(e.target.value));
                                }}
                            >
                                {[10, 20, 30, 40, 50].map(pageSize => (
                                    <option key={pageSize} value={pageSize}>
                                        Show {pageSize}
                                    </option>
                                ))}
                            </select>
                        </div>
                    </div>

                    <br />
                    <div>Showing the first 20 results of {rows.length} rows</div>
                    <div>
                        <pre>
                            <code>{JSON.stringify(state.filters, null, 2)}</code>
                        </pre>
                    </div>
                </div>

            </>
        );
    }

    const columns = React.useMemo(
        () => [
            {
                Header: "Name",
                columns: [
                    {
                        Header: "Start Timestamp",
                        accessor: "starttimestamp",
                    },
                    {
                        Header: "End Timestamp",
                        accessor: "endtimestamp",
                    },
                    {
                        Header: "App ID",
                        accessor: "app id",
                        filter: "equals"
                    },
                    {
                        Header: "Customer Name",
                        accessor: "customername",
                    },
                    {
                        Header: "Entity Type",
                        accessor: "entitytype",
                    },
                    {
                        Header: "Entity Name",
                        accessor: "entityname",
                    },
                    {
                        Header: "Duration",
                        accessor: "duration",
                    },
                    {
                        Header: "Agent Status",
                        accessor: "agentstatus",
                    }
                ]
            }
        ],
        []
    );

    return (
        <>
            {props?.callHistoryList && props?.callHistoryList?.length > 0
                ? <Table columns={columns} data={props?.callHistoryList} />
                :
                <>
                    <Table columns={columns} data={[]} />
                    <NoData
                        title="No records found"
                        subtitle="We couldn't find any records here."
                        pageName="schedule"
                        img="../images/icon-paper.svg"
                    />
                </>
            }
        </>

        // <table className="table">
        //     <thead>
        //         <tr>
        //             <th scope="col">Start Timestamp</th>
        //             <th scope="col">End Timestamp</th>
        //             <th scope="col">App ID</th>
        //             <th scope="col">Customer Name</th>
        //             <th scope="col">Entity Type</th>
        //             <th scope="col">Entity Name</th>
        //             <th scope="col">Duration</th>
        //             <th scope="col">Agent Status</th>
        //         </tr>
        //     </thead>
        //     <tbody>
        //         {props?.callHistoryList?.length > 0
        //             ? props?.callHistoryList?.map((item, i) => <tr key={i}>
        //                 <td>{item?.starttime}</td>
        //                 <td>{item?.endtime}</td>
        //                 <td>{item?.appid}</td>
        //                 <td>{item?.custname}</td>
        //                 <td>{item?.entitytype}</td>
        //                 <td>{item?.entityname}</td>
        //                 <td>{item?.duration}</td>
        //                 {item?.agentstatus === '1' && <td>Approved</td>}
        //                 {item?.agentstatus === '2' && <td>Rejected</td>}
        //                 {item?.agentstatus === '3' && <td>Issued</td>}
        //                 {item?.agentstatus === '' && <td>-</td>}
        //             </tr>)
        //             : (<tr><td colSpan={5} className='text-center'>No VCIPID's Found.</td></tr>)}
        //     </tbody>
        // </table>
    )
}

export default CallHistoryCmp;