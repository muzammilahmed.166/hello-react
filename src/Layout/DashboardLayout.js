import React, { useEffect, useState } from 'react';
import { Outlet, useLocation, useNavigate } from 'react-router-dom';
import Header from '../Pages/Common/Header';
import RouteNames from '../Constants/RouteNames';
import Activity from '../Pages/Dashboard/Activity/Activity';
import { endVideoCallByAgentSagaAction } from '../Store/SagaActions/CommonSagaActions';
import { useDispatch } from 'react-redux';

const DashboardLayout = () => {
    const navigate = useNavigate();
    const location = useLocation();

    const dispatch = useDispatch();
    
    useEffect(() => {
        dispatch(endVideoCallByAgentSagaAction(false));
    }, [])
    


    return (
        <>
            <Header />
            <main className="main">
                <div className="container-fluid mt-2">

                <div className="row m-0 h-100">
                    <div className="col-md-3 p-2">
                        <div className="bx">
                            <Activity />
                        </div>
                    </div>
                    <div className="col-md-9 p-2">
                        <div className="bx">
                            <ul className="nav nav-tabs custom-dshbd-tabs" id="myTab" role="tablist">
                                <li className="nav-item" role="presentation">
                                    <button
                                        className={`nav-link ${location?.pathname === RouteNames.DASHBOARD_LIVE  || location?.pathname === RouteNames.DASHBOARD_SCHEDULE ||  location?.pathname === RouteNames.DASHBOARD_ASSIGNED ? 'active' : ''}`}
                                        onClick={() => navigate(RouteNames.DASHBOARD_LIVE)}
                                        id="queue-tab" data-bs-toggle="tab"
                                        data-bs-target="#queue" type="button"
                                        role="tab" aria-controls="home" aria-selected="true">
                                        <img src='../../images/icon-queue.svg' className='me-1' alt='' />
                                        Customer Queue
                                    </button>
                                </li>
                                <li className="nav-item" role="presentation">
                                    <button
                                        className={`nav-link ${location?.pathname === RouteNames.CALLHISTORY ? 'active' : ''}`}
                                        onClick={() => navigate(RouteNames.CALLHISTORY)}
                                        id="history-tab"
                                        data-bs-toggle="tab"
                                        data-bs-target="#history"
                                        type="button" role="tab"
                                        aria-controls="profile"
                                        aria-selected="false">
                                        <img src='../../images/icon-history.svg' className='me-1' alt='' />
                                        Call history
                                    </button>
                                </li>
                            </ul>
                            <div className="tab-content customer-table" id="myTabContent">
                                <Outlet />
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </main>
        </>
    )
}

export default DashboardLayout;