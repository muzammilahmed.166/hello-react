
import React, { useState } from 'react';
import AddCustomerFormModel from './AddCustomerFormModal';
import PortalReportModal from '../../Portals/PortalReportModal';

const AddCustomerModel = (props) => {
  const [isOpen, setIsOpen] = useState(false);
  const [AddCustomerForm, setAddCustomerForm] = useState(false);
  const [isClose, setIsClose] = useState(false)
  

  console.log(isClose, "isClose")



  const toggleAddCustomerModel = () => {
    setIsOpen(true);
    setIsClose(true);
    console.log(isClose, "isClose")


  }
  const closeModal2 = () => {
    setIsOpen(!isOpen);
  }


  const AddNewCustomerForm = () => {
    setIsOpen(false);
    setAddCustomerForm(true);
  }


  return (
    <>
      {/* <div class="modal fade" id="exampleModalAddoption" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md modal-dialog-centered">
      <div class="modal-content"> */}
      <div class="modal-header">
        <h5 class="modal-title m-auto" id="exampleModalLabel">Add Customer</h5>
        <button type="button" class="close" onClick={props?.closeModal} >
          <i class="fa-solid fa-xmark"></i>
        </button>        </div>
      <div class="modal-body add-customer-model">
        <p class="drag-drop">Choose an <b class="text-dark">‘option’ </b>to add a new customer</p>


        <div class="display-addcustomer">
          <div class="div-addcustomer"><div class="drop-zoneview">
            <img src="../../images/users.svg" class="folder-users" />
            <p class="upload-form" onClick={toggleAddCustomerModel}>Individual form</p>
            <input type="file" name="myFile" class="drop-zone__inputboxes" placeholder="Individual form" />
          </div></div>
          <div class="div-addcustomer m-auto upload-button">or</div>
          <div class="div-addcustomer"><div class="drop-zoneview1">
            <img src="../../images/group.svg" class="folder-users" />
            <p class="upload-form"> Bulk upload</p>

            <input type="file" name="myFile" class="drop-zone__inputboxes" placeholder="Bulk upload" onClick={toggleAddCustomerModel} />
          </div></div>
        </div>
   
        <div class="mb-4"></div>
      </div>
      {/* <PortalReportModal isOpen={isOpen} isSize={"lg"}>
        <AddCustomerFormModel closeModal2={closeModal2} AddNewCustomerForm={AddNewCustomerForm} />
      </PortalReportModal> */}
      
      {/* </div>
    </div>
  </div> */}
    </>
  )
}




export default AddCustomerModel;





