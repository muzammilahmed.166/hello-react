import React from 'react';

const AadhaarNumberModal = (props) => {
    return (
        <>
            <div className="modal-header align-items-center">
                <h5 className="modal-title text-center w-100" id="exampleModalLabel" style={{ visibility: 'hidden' }}>Aadhaar
                </h5>
                <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={props.closeModal}>
                    <img src="../images/icon-close.svg" alt="close" />
                </button>
            </div>
            <div className="modal-body">
                <h4 className="title text-center">Aadhaar number</h4>
                <p className="txt text-center">
                    Enter your 12 digit Aadhaar number to begin
                </p>
                <form className="app-body-frm" action>
                    <div className="frm-grp">
                        <input type="number" name className="frm-grp-inp" placeholder="Aadhaar Number*" required />
                    </div>
                    <div className="frm-grp">
                        <input type="number" name className="frm-grp-inp" placeholder="Enter Security Code*" required />
                    </div>
                    <p className="txt text-left">
                        Type the character you see in the picture
                    </p>
                </form>
            </div>
            <div className="modal-footer d-flex">
                {/* <button type="button" className="btn w-auto btn-white" data-dismiss="modal">Close</button> */}
                <button type="button" className="btn w-auto" onClick={props.sendOTPModal}>Send OTP</button>
            </div>
        </>
    )
}

export default AadhaarNumberModal;