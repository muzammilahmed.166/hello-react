import React, { useEffect, useState } from 'react'
import { useDispatch,useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import AssignedCmp from '../../../Components/DashboardCmp/CustomerQueue/AssignedCmp';
import StatusCodes from '../../../Constants/StatusCodes';
import { getDashboardLiveSagaAction } from '../../../Store/SagaActions/AgentDashboardSagaActions';
import { joinVideoSessionAction } from '../../../Store/SagaActions/InitiateVideoCallSagaActions';
import NoCustomer from '../NoCustomer';
import RouteNames from '../../../Constants/RouteNames';

const Assigned = () => {
    const [liveList, setLiveList] = useState([]);
    // const [isLoading, setIsLoading] = useState(false);

    const isLoading = useSelector(state => state.HomeReducer.apiStatus);

    

    // const [spinner, setSpinner] = useState(false);
    // const [vcipID, setVcipID] = useState('');

    const dispatch = useDispatch();
    const navigate = useNavigate();

    let intervalId;
    let vcipData;

    useEffect(() => {
        getLiveList();
        intervalId = setInterval(() => {
            getLiveList();
        }, 5000);
        return () => {
            clearIntervalLive();
        }
    }, []);

    const clearIntervalLive = () => {
        if (intervalId) {
            clearInterval(intervalId)
        }
    }

    const getLiveList = () => {
        const model = {
            listtype: '1' // -> 1-live, 2-scheduel
        }
        // setIsLoading(true);
        dispatch(getDashboardLiveSagaAction({ model: model, callback: getLiveData }));
    }

    const getLiveData = (data) => {
        // setLiveList(data);
        if (data?.respcode == "200") {

            let LiveListData = []
            if (data?.vciplist?.length > 0) {
                data?.vciplist?.map((item, i) => {
                    LiveListData.push({
                        "customer name": item?.name, "app id": item?.appid, "location": item?.customerloc, "ip status": item?.customeripstatus, "waiting since": item?.wtime, "status": item?.joinstatus === StatusCodes.VCIP_JOINSTATUS_ENABLE
                            ? <button className='tab-btn' onClick={() => joinVideoSession(item)} >Join Now {isLoading ? <span className="spinner"></span> : null}</button>
                            : <button className='tab-btn' disabled>Join Now</button>

                    })
                    //         <button type="button" className="btn btn-primary w-20" data-bs-toggle="modal" data-bs-target="#viewKycReportModal">
                    //             View Kyc Report
                    //         </button>
                    // })
                })
                // {isLoading !== 0 && <div className='pageloader'></div>}




            } else {
                console.log(
                    "lsdkfj"
                )

            }
            setLiveList(LiveListData)
        } else if (data.respcode == "4003") {
            navigate(RouteNames.LOGIN);
            window.location.reload();
        }
    }

    const joinVideoSession = (vcipData) => {
        const vcipID = vcipData?.videoconfsessionid;
        sessionStorage.setItem('vcipkey', vcipData?.vcipkey);
        const location = sessionStorage.getItem('location');
        const geolocation = sessionStorage.getItem('geolocation');
        const ip = sessionStorage.getItem('ip');
        const connection = navigator.connection || navigator.mozConnection || navigator.webkitConnection;
        const model = {
            "vcipkey": vcipData?.vcipkey,
            "custdetails": {
                "ip": "10.10.10.10",
                "location": "",
                "geolocation": "",
                "nw_incoming": "",
                "nw_outgoing": "",
                "videoresolution": ""
            },
            "agentdetails": {
                "ip": ip,
                "location": location,
                "geolocation": geolocation,
                "nw_incoming": connection.downlink,
                "nw_outgoing": `${connection.rtt}, ${connection.effectiveType === "4g" ? connection.effectiveType : "Low Speed"}`,
                "videoresolution": `${window.screen.width} * ${window.screen.height}`
            }

        }
        // setIsLoading(true);
        dispatch(joinVideoSessionAction({ model: model, callback: joinVideoSessionData, vcipID: vcipID }));
    }

    const joinVideoSessionData = (vcipID) => {
        // setIsLoading(true);
        navigate('/video/' + vcipID);
    }

    return (
        <><NoCustomer
                    title="No customer in Queue!"
                    subtitle="You don't have any customer in Queue"
                    img="../images/manager.svg"
                    pageName="Assigned"
                    button = "Add new customer"
                />
        </>
    )
}

export default Assigned;