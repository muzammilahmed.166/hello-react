import React from 'react';
import { Outlet, useLocation, useNavigate } from 'react-router-dom';
import RouteNames from '../../Constants/RouteNames';
import { useSelector } from 'react-redux';

const Dashboard = () => {
    const count = useSelector(state => state.HomeReducer)

    const navigate = useNavigate();
    const location = useLocation();
    // const getLiveList = () => {
    //     const model = {
    //         listtype: '1' // -> 1-live, 2-scheduel
    //     }
    //     // setIsLoading(true);
    //     dispatch(getDashboardLiveSagaAction({ model: model, callback: getLiveData }));
    // }

    // console.log(count,"count")

    return (
        <>
            <div className="tab-pane fade show active mt-4" id="queue" role="tabpanel" aria-labelledby="queue-tab">
                <div className='d-flex align-items-center my-2'>
                    {/* <NavLink className={(navData) => navData?.isActive ? 'btn w-auto' : 'btn w-auto btn-white'} to='/dashboard/live'>Live</NavLink> */}
                    <button
                        className={`btn w-auto me-2 ${location?.pathname === RouteNames.DASHBOARD_LIVE ? '' : 'btn-white'}`}
                        onClick={() => navigate(RouteNames.DASHBOARD_LIVE)}>
                        {/* <img src='../../images/Vector-video.svg' className='me-1' alt='' /> */}
                        <i class="fa-solid fa-video"></i> Live ({count?.liveListCount ? count?.liveListCount : 0})
                    </button>
                    <button
                        className={`btn w-auto me-2 ${location?.pathname === RouteNames.DASHBOARD_SCHEDULE ? '' : 'btn-white'}`}
                        onClick={() => navigate(RouteNames.DASHBOARD_SCHEDULE)}>
                        {/* <img src='../../images/Vector-video.svg' className='me-1' alt='' /> */}
                        <i class="fa-solid fa-clock"></i>  Schedule ({count?.scheduleListCount ? count?.scheduleListCount : 0})
                    </button>
                    <button
                        className={`btn w-auto me-2 ${location?.pathname === RouteNames.DASHBOARD_ASSIGNED ? '' : 'btn-white'}`}
                        onClick={() => navigate(RouteNames.DASHBOARD_ASSIGNED)}>
                        {/* <img src='../../images/Vector-video.svg' className='me-1' alt='' /> */}
                        <i class="fa-solid fa-user-check"></i> Assigned ({count?.scheduleListCount ? count?.scheduleListCount : 0})
                    </button>
                    {/* <button className={`btn w-auto btn-white ${(navData) => navData?.isActive ? '' : 'btn-white'}`}>Assigned</button> */}
                </div>
                <Outlet />
            </div>
        </>
    )
}

export default Dashboard;