import React, { useState } from 'react';
// import AddCustomerModel from '../Modals/AddCustomerModel';
import AddCustomerFormModel from '../Modals/AddCustomerFormModal';
import PortalReportModal from '../../Portals/PortalReportModal';

const NoCustomer = (props) => {
    const [isOpen, setIsOpen] = useState(false);
    // const [AddCustomer, setAddCustomer] = useState(false);
    // const [isClose, setIsClose] = useState(false);


    const closeModal = () => {
        setIsOpen(!isOpen)
        // setIsClose(!isClose);

    }

    const toggleCustomerDrop = () => {
        setIsOpen(!isOpen);
    }
    const AddNewCustomer = () => {
        setIsOpen(false);
        // setAddCustomer(true);
    }

    return (
        <>
            <article className='nodata'>
                <img src={props?.img} className='mb-3' alt='' />
                <h4 className='title'>{props?.title}</h4>
                <p className='txt'>{props?.subtitle}</p>
                <div className="add-new-bx">
                    <button className='add-new-customer-button' onClick={toggleCustomerDrop}>
                        <i class="fa fa-plus" aria-hidden="true"></i>{props?.button}</button>
                </div>
            </article>
            {/* <PortalReportModal isOpen={isOpen} isSize={"lg"}>
                <AddCustomerModel closeModal={closeModal}  AddNewCustomer={AddNewCustomer} />
        </PortalReportModal> */}
            <PortalReportModal isOpen={isOpen} isSize={"lg"}>
                <AddCustomerFormModel  closeModal={closeModal} closeModalChild={closeModal} AddNewCustomer={AddNewCustomer} />
            </PortalReportModal>


        </>


    )
}

export default NoCustomer;