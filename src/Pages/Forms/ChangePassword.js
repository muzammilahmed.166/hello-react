import React, { useEffect, useState } from 'react'
import ChangePasswordCmp from '../../Components/FormCmp/ChangePasswordCmp'
import { useDispatch } from 'react-redux';
import { useNavigate, useParams } from 'react-router-dom';
import base64 from 'base-64';
import LoginCmp from '../../Components/FormCmp/LoginCmp';
import RouteNames from '../../Constants/RouteNames';
import { getRolesSagaAction, resetSagaAction, resetPasswordSagaAction } from '../../Store/SagaActions/AuthSagaActions';
import toast from 'react-hot-toast';


const ChangePassword = () => {
    const [changepasswordObj, setChangePasswordObj] = useState({
        newpassword: '',
        retypepassword: '',
    });

    const naviage = useNavigate();
    const dispatch = useDispatch();
    const params = useParams();

    // useEffect(() => {
    //     dispatch(getRolesSagaAction({ callback: getRolesData }));
    // }, []);

    // const getRolesData = (data) => {
    //     setRoles(data)
    // }
    const handleChange = (e) => {
        const { name, value } = e.target;
        console.log(e.target.value, "e.target.value)")
        // setChangePasswordObj({ ...changepasswordObj, [name]: value });
        setChangePasswordObj((prevState) => {
            return {
                ...prevState,
                [name]: value,
            };

        })
    }

    // useEffect(() => {
    //     const model = {
    //         vcipref: params?.token,
    //     }
    //     dispatch(actionGetVcipDetails({ model: model, callback: getVcipDetailsData }));
    // }, []);
    const changepasswordBtn = (e) => {
        e.preventDefault();
        var encodedString = btoa(changepasswordObj.newpassword);
        console.log(encodedString, "encodedString")
        // let buff = new Buffer(changepasswordObj.newpassword);
        // let stringToBase64 = buff.toString('base64');
        // console.log(stringToBase64, 'stirn gto base')
        if (changepasswordObj) {
            const model = {
                "new_pwd": encodedString,
                "token": params?.token,

                // "token":params?.id,
            }
            dispatch(resetPasswordSagaAction({ model: model, callback: changepasswordRespData }))
            naviage(RouteNames.LOGIN)
        } else {
            toast.error('Please check all inputs')
        }
    }

    const changepasswordRespData = (data) => {
        console.log(data, 'dat ')
        // naviage(RouteNames.LIVE);
        // toast.success('Successfully sent Reset link to the User email')


    }
    return (
        <>
            <ChangePasswordCmp
                handleChange={handleChange}
                changepassword={changepasswordBtn}
            />
        </>
    )
}

export default ChangePassword;