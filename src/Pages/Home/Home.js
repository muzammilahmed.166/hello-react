import React, { useEffect, useState } from 'react'
import toast, { Toaster } from 'react-hot-toast';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate, useParams } from 'react-router-dom';
import HomeCmp from '../../Components/HomeCmp';
import RouteNames from '../../Constants/RouteNames';
import { actionLocationDetails } from '../../Store/SagaActions/CommonSagaActions';
import { actionCreateSlot, actionGetVcipDetails } from '../../Store/SagaActions/GetVcipDetailsSagaActions';
import AppFooter from '../Common/AppFooter';
import Loader from '../Common/Loader';
import publicIp from 'public-ip';

const Home = () => {

    const [ip, setIp] = useState("");
    const [lat, setLat] = useState("");
    const [long, setLong] = useState("");
    const [locationName, setLocationName] = useState("");
    const [customerVcipDetails, setCustomerVcipDetails] = useState({})

    const dispatch = useDispatch();
    const navigate = useNavigate();
    const params = useParams();

    // const isLoading = useSelector(state => state.HomeReducer.apiStatus);

    useEffect(() => {
        const model = {
            vcipref: params?.id,
        }
        dispatch(actionGetVcipDetails({ model: model, callback: getVcipDetailsData }));
    }, []);

    const getVcipDetailsData = (data) => {
        // console.log("============", data);
        setCustomerVcipDetails(data);
        getLatLong();
    }

    const getLatLong = () => {
        if (navigator.geolocation) {
            // navigator.geolocation.getCurrentPosition((this.showPosition));
            navigator.geolocation.getCurrentPosition(
                (position) => {
                    // alert(position.coords.latitude, position.coords.longitude);
                    if (position.coords.latitude) {
                        setLat(position.coords.latitude)
                        setLong(position.coords.longitude)
                        const model = {
                            lat: position.coords.latitude,
                            long: position.coords.longitude
                        };
                        dispatch(actionLocationDetails({ model: model, callback: getLocationDetailsData }));
                    } else {
                        toast.error("Please enable location")
                    }
                },
                (error) => {
                    console.log(error);
                }, {
                enableHighAccuracy: true
                , timeout: 5000
            });
        } else {
            toast.warn("Geolocation is not supported by this browser.");
        }
        (async () => {
            setIp(await publicIp.v4());
        })();
    }

    const getLocationDetailsData = (data) => {
        setLocationName(data);
    }

    const startProcess = () => {
        const vcipkey = sessionStorage.getItem('vcipkey');
        const model = {
            vcipkey: vcipkey,
            custdetails: {
                ip: ip,
                location: lat + "," + long,
                geolocation: locationName,
                nw_incoming: "",
                nw_outgoing: "",
                videoresolution: ""
            },
            ref1: ""
        }
        dispatch(actionCreateSlot({
            model: model, callback: (data) => {
                sessionStorage.setItem('ip', ip);
                sessionStorage.setItem('location', lat + "," + long);
                sessionStorage.setItem('geolocation', locationName);
                if (customerVcipDetails?.kycstatus === '0') {
                    navigate(RouteNames.SELECT_KYC_PROCESS, { replace: true });
                } else if (customerVcipDetails?.panstatus === '0') {
                    navigate(RouteNames.PAN_KYC_PROCESS, { replace: true });
                } else if (customerVcipDetails?.videocallstatus === '0') {
                    navigate(RouteNames.SELECT_VIDEOCALL_LANGAUAGE, { replace: true });
                }
            }
        }))
    }

    return (
        <>
            {/* {isLoading !== 0 ? <Loader /> : null} */}
            <HomeCmp />
            <AppFooter
                btnName="Get Started"
                navigate={startProcess}
            />
        </>
    )
}

export default Home;