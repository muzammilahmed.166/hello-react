import { OpenVidu } from 'openvidu-browser';
import React, { useContext, useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate, useParams } from 'react-router-dom';
import InitiatingVideoCallCmp from '../../../Components/VideoCallCmp/InitiatingVideoCallCmp'
import RouteNames from '../../../Constants/RouteNames';
import { createVideoSessionSagaAction } from '../../../Store/SagaActions/VideoSagaActions';
import OpenViduVideoComponent from './OvVideo';
// import AppFooter from '../../Common/AppFooter';
import VideoSession from './VideoSession';
// import './video.css';
import { actionNoficationListSaga } from '../../../Store/SagaActions/CommonSagaActions';
import UserVideoComponent from './UserVideoComponent';
import { VCIPDetailsContext } from '../VideoDashboard';

const InitiatingVideoCall = () => {
  const [session, setSession] = useState(undefined);
  const [subscribers, setSubscribers] = useState([]);
  const [mainStreamManager, setMainStreamManager] = useState(undefined);
  const [publisher, setPublisher] = useState(undefined);

  const { customerVcipDetails, stop, openIssueModal } = useContext(VCIPDetailsContext);

  const dispatch = useDispatch();
  const params = useParams();
  const isCallEnded = useSelector(state => state.HomeReducer.isCallEnded);

  let OV;
  var notificationIntervalId;

  useEffect(() => {
    joinVideoSession();
    return () => {
      endVideoCall();
    }
  }, []);

  useEffect(() => {
    if (isCallEnded) {
      if (session) {
        endVideoCall()
      }
    }
  }, [isCallEnded])

  const joinVideoSession = () => {
    OV = new OpenVidu();
    var mySession = OV.initSession();
    setSession(mySession);
    // var mySession = session;
    mySession.on('streamCreated', (event) => {
      var subscriber = mySession.subscribe(event.stream, undefined);

      if (event.stream.typeOfVideo !== "SCREEN") {
        sessionStorage.setItem("connectionId", event.stream.connection.connectionId);
      }
      var subscriberArr = subscribers;
      subscriberArr.push(subscriber);
      sessionStorage.setItem("subscribers", Object.keys(subscriberArr))
      setSubscribers(subscriberArr);
    });
    mySession.on('streamDestroyed', (event) => {
      deleteSubscriber(event.stream.streamManager);
    });

    const name = params.id;
    const model = {
      name: name,
      sessionId: name,
      OV: OV,
      session: mySession,
      myUserName: name
    }
    dispatch(createVideoSessionSagaAction({ model: model, callback: getCreateSessionData }));
  }

  // DELETE VIDEO CALL SUBSCRIBER
  const deleteSubscriber = (streamManager) => {
    let subscribersData = subscribers;
    let index = subscribersData.indexOf(streamManager, 0);
    if (index > -1) {
      subscribersData.splice(index, 1);
      setSubscribers(subscribersData);
    }
  }

  const getCreateSessionData = (data) => {
    setMainStreamManager(data.mainStreamManager)
    setPublisher(data.publisher)
  }

  // END VIDEO CALL
  const endVideoCall = () => {
    if (session) {
      session.disconnect();
    }
    sessionStorage.removeItem("session");
    sessionStorage.removeItem("connectionId");
    sessionStorage.removeItem("videoconfsessionid");
    sessionStorage.removeItem("publisher");
    sessionStorage.removeItem("session");
  }

  console.log("op->->subscribers", subscribers, "op->->mainStreamManager", mainStreamManager, "op->->session", session);

  return (
    <>
      {!session
        ? <InitiatingVideoCallCmp />
        : <>
          <div className="row m-0 flex-1">
            <div className="col-6 ps-0">
              <div className="vd-bx">
                <div className="vd-bx-agnt">
                  <span className='vd-name'>Agent</span>
                  {mainStreamManager ? (
                    <div id="cusomer" className='h-100'>
                      <OpenViduVideoComponent streamManager={mainStreamManager} />
                    </div>
                  ) : null}
                </div>
                {subscribers?.length > 0 ? null
                  : <p className="text-center text-danger">
                    not Available
                  </p>
                }
                <ul className="vd-bx-list-icons">
                  <li>
                    <button className="vd-bx-list-icon-btn">
                      <img src="../../images/icon-user.svg" alt="" />
                    </button>
                    <div className="vd-bx-info">
                      <h6 className='vd-bx-info-ttl'>Customer details</h6>
                      <hr />
                      <ul className="vd-bx-info-lst">
                        <li><span>Name:</span> {customerVcipDetails?.customerdetails?.name}</li>
                        <li><span>Father name:</span> {customerVcipDetails?.customerdetails?.fname} </li>
                        <li><span>Date of birth:</span> {customerVcipDetails?.customerdetails?.dob}</li>
                        <li><span>Gender:</span> {customerVcipDetails?.customerdetails?.gender}</li>
                        <li><span>Email id:</span> {customerVcipDetails?.customerdetails?.email}</li>
                        <li><span>Mobile number:</span> {customerVcipDetails?.customerdetails?.mobile}</li>
                        <li><span>Current location:</span> {customerVcipDetails?.customerdetails?.curr_address}</li>
                      </ul>
                    </div>
                  </li>
                  <li>
                    <button className="vd-bx-list-icon-btn">
                      <img src="../../images/Vector-location.svg" alt="" />
                    </button>
                    <div className="vd-bx-info">
                      <h6 className='vd-bx-info-ttl'>Customer location</h6>
                      <hr />
                      <ul className="vd-bx-info-lst">
                        <li><span>Lat & Long:</span> {customerVcipDetails?.customerdetails?.location}</li>
                        <li><span>Location:</span> {customerVcipDetails?.customerdetails?.geo_location}</li>
                        <li><span>Address</span> {customerVcipDetails?.customerdetails?.per_address}</li>
                        <li><span>IP Address</span> {customerVcipDetails?.customerdetails?.ipaddress}</li>
                      </ul>
                    </div>
                  </li>
                  <li>
                    <button className="vd-bx-list-icon-btn">
                      <img src="../../images/Vector-network.svg" alt="" />
                    </button>
                    <div className="vd-bx-info">
                      <h6 className='vd-bx-info-ttl'>Agent speed network</h6>
                      <hr />
                      <ul className="vd-bx-info-lst d-flex justify-content-around align-items-center">
                        <li className='text-center'> {customerVcipDetails?.customerdetails?.nw_incoming}<br />  <span>Incoming</span></li>
                        <li className='text-center'>{customerVcipDetails?.customerdetails?.nw_outgoing} <br /> <span>Outgoing</span>  </li>
                      </ul>
                      <h6 className='vd-bx-info-ttl'>Customer speed network</h6>
                      <hr />
                      <ul className="vd-bx-info-lst d-flex justify-content-around align-items-center mt-2">
                        <li className='text-center'> {customerVcipDetails?.customerdetails?.nw_incoming}<br />  <span>Incoming</span></li>
                        <li className='text-center'>{customerVcipDetails?.customerdetails?.nw_outgoing} <br /> <span>Outgoing</span>  </li>
                      </ul>
                    </div>
                  </li>
                  <li>
                    <button className="vd-bx-list-icon-btn">
                      <img src="../../images/Vector-design.svg" alt="" />
                    </button>
                    <div className="vd-bx-info">
                      <h6 className='vd-bx-info-ttl'>Agent Video resolution</h6>
                      <hr />
                      <ul className="vd-bx-info-lst mt-2">
                        <li className='text-center'> {window.screen.width} x {window.screen.height} <br /><span>Pixels</span> </li>
                      </ul>
                      <h6 className='vd-bx-info-ttl'>Customer video resolution</h6>
                      <hr />
                      <ul className="vd-bx-info-lst">
                        <li className='text-center'> {customerVcipDetails?.customerdetails?.videoresolution} <br /> <span>Pixels</span></li>
                      </ul>
                    </div>
                  </li>
                  <li>
                    <button className="vd-bx-list-icon-btn" onClick={openIssueModal} >
                      <img src="../../images/Vector-error.svg" alt="" />
                    </button>
                  </li>
                </ul>
              </div>
              {/* <button className='btn' onClick={leaveSession}>End Call</button> */}
            </div>
            <div className="col-6">
              <div className="vd-bx h-100">
                <div className="vd-bx-agnt h-100">
                  <span className='vd-name'>Customer</span>
                  {subscribers?.map((sub, i) => {
                    if (sub.stream.typeOfVideo !== "SCREEN") {
                      return <div key={i} className="stream-container othervideo cstmr-vd-call h-100">
                        <UserVideoComponent streamManager={sub} />
                      </div>
                    }
                  })}
                </div>
              </div>
            </div>
          </div>
          <canvas id="canvas" style={{ objectFit: 'fill', position: "absolute", width: "100%", height: "100%", display: "none" }}></canvas>
        </>
      }


    </>
  )
}

export default InitiatingVideoCall;