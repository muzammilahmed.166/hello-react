import React from 'react'
import OpenViduVideoComponent from './OvVideo'

const VideoSession = (props) => {
    return (
        <>
            {/* <div className="myvideo">
                {props?.mainStreamManager !== undefined ? (
                    <div id="main-video">
                        <OpenViduVideoComponent streamManager={props?.mainStreamManager} />
                    </div>
                ) : null}
            </div> */}
            {props?.streamManager !== undefined ? (
                <div className="streamcomponent">
                    <OpenViduVideoComponent streamManager={props?.streamManager} />
                    {/* <div><p>{this.getNicknameTag()}</p></div> */}
                </div>
            ) : null}

            {/* {props?.subscribers?.map((sub, i) => {
                if (sub.stream.typeOfVideo !== "SCREEN") {
                    return <div key={i} className="stream-container othervideo">
                        <OpenViduVideoComponent streamManager={sub} />
                    </div>
                }
            })} */}
        </>
    )
}

export default VideoSession