import { OpenVidu } from 'openvidu-browser';
import React, { useContext, useEffect, useState } from 'react'
import { useDispatch } from 'react-redux';
import { useSelector } from 'react-redux';
import { useNavigate, useParams } from 'react-router-dom';
import Axios from 'axios';
import base64 from 'base-64';
import AES256 from 'aes-everywhere';
import toast from 'react-hot-toast';

import OpenViduVideoComponent from './OvVideo';
import InitiatingVideoCallCmp from '../../../Components/VideoCallCmp/InitiatingVideoCallCmp'
import RouteNames from '../../../Constants/RouteNames';
import { createVideoSessionSagaAction, endVideoCallSagaAction, updateVcipStatusSagaAction } from '../../../Store/SagaActions/VideoSagaActions';
// import AppFooter from '../../Common/AppFooter';
import VideoSession from './VideoSession';
import './video.css';
import { actionNoficationListSaga, pushNotificationSaga } from '../../../Store/SagaActions/CommonSagaActions';
import { VCIPDetailsContext } from '../VideoDashboard';

let KEY = 'f379e0b661ae4650b19169e4d93665dc';

const aesEncrypt = (data) => {
  var passphrase = KEY;
  let val1 = passphrase.substr(0, 4);
  let val2 = passphrase.substr(passphrase.length, 4);
  let updatedValue = val1 + passphrase + val2;
  const finalvalue = base64.encode(updatedValue).substr(0, 32);
  const encrypted = AES256.encrypt(JSON.stringify(data), finalvalue);
  return encrypted;
}

const aesDecrypt = (data) => {
  var passphrase = KEY;
  let val1 = passphrase.substr(0, 4);
  let val2 = passphrase.substr(passphrase.length, 4);
  let updatedValue = val1 + passphrase + val2;
  const finalvalue = base64?.encode(updatedValue).substr(0, 32);
  const decrypted = AES256?.decrypt(data, finalvalue);
  return decrypted;
}

const instance = Axios.create({
  // baseURL: OPENVIDU_SERVER_URL,
  // baseURL: 'https://vcip.syntizen.com:3002/vkycrestservices/',
  baseURL: 'https://preprodvcip.syntizen.com:3002/vkycrestservices/',
});

const InitiatingVideoCall = () => {
  const [session, setSession] = useState(undefined);
  const [session2, setSession2] = useState(undefined)
  const [subscribers, setSubscribers] = useState([]);
  const [mainStreamManager, setMainStreamManager] = useState(undefined);
  const [publisher, setPublisher] = useState(undefined);
  // const [notificationIntervalId, setNotificationIntervalId] = useState('');
  const [notificationList, setNotificationList] = useState({});
  const [displayQtn, setDisplayQtn] = useState({});
  // const [OV, setOV] = useState(undefined);

  const navigate = useNavigate();
  const dispatch = useDispatch();
  // const session2 = useSelector(state => state.VcipReducer.session2)

  const params = useParams();

  const { customerVcipDetails } = useContext(VCIPDetailsContext);

  // var OV;
  var notificationIntervalId;
  var OV = new OpenVidu();
  var mySession = OV.initSession();

  useEffect(() => {
    // let ovData = new OpenVidu();
    // let mySession = ovData.initSession();
    // setSession(mySession);
    // setOV(ovData);
    joinVideoSession();
    return () => {
      leaveSession();
      // endVideoCall();
      // clearNotificationInterval();
      // leaveCallWithoutEndSession();
    }
  }, []);

  useEffect(() => {
    if (session && Object.keys(session)?.length > 0) {
      console.log("=====>shdahag", session);
      joinVideoSession();
    }

  }, [session])

  const clearNotificationInterval = () => {
    console.log("notificationIntervalId-->", notificationIntervalId);
    if (notificationIntervalId) {
      clearInterval(notificationIntervalId)
    }
  }

  const joinVideoSession = () => {
    // OV = new OpenVidu();
    // var mySession = OV.initSession();
    // var mySession = session;
    sessionStorage.setItem("mySession", mySession);
    // setSession(mySession)
    mySession.on('streamCreated', (event) => {
      var subscriber = mySession.subscribe(event.stream, undefined);
      // console.log("==================",event.stream.connection.connectionId);
      if (event.stream.typeOfVideo !== "SCREEN") {
        sessionStorage.setItem("connectionId", event.stream.connection.connectionId);
      }
      var subscribersData = subscribers ? subscribers : [];
      subscribersData.push(subscriber);
      sessionStorage.setItem("subscribers", Object.keys(subscribers))
      setSubscribers(subscribersData)
      // this.setState({
      //   subscribers: subscribers,
      // });
    });
    mySession.on('streamDestroyed', (event) => {
      deleteSubscriber(event.stream.streamManager);
    });
    const name = params.id;
    // const model = {
    //   name: name,
    //   sessionId: name,
    //   OV: OV,
    //   session: session,
    //   myUserName: name
    // }
    createSessionAction(name, mySession, OV);
  }

  const createSessionAction = (sessionId, mySession, ovData) => {
    const data = {
      "recordingMode": "ALWAYS",
      "customSessionId": sessionId,
      "defaultRecordingLayout": "CUSTOM",
    }
    let body = {
      "data": aesEncrypt(data)
    }
    instance.post('/sessions', body, {
      headers: {
        'Content-Type': 'application/json',
      },
    }).then((resp) => {
      var response = aesDecrypt(resp.data.data);
      console.log("================sessions    sessions---", response);
      createTokenAction(sessionId, mySession, ovData);
      // createRecording(sessionId, mySession, ovData);
      // dispatch(chatCreateToken(sessionId, $this, OV));
    }).catch((err) => {
      try {
        var error = Object.assign({}, aesDecrypt(err.data.data));
        if (error.response && error.response.status === 409) {
          createTokenAction(sessionId, mySession, ovData);
          // createRecording(sessionId, mySession, ovData);
          // dispatch(chatCreateToken(sessionId, $this, OV));
        } else {
          toast.error("No connection to VCIP");
        }
      } catch {
        toast.warn("No connection to VCIP")
      }
    });
  }

  const createTokenAction = (sessionId, session, ovData) => {
    var data = {
      session: sessionId
    };
    let body = {
      "data": aesEncrypt(data)
    }
    instance.post('/tokens', body, {
      headers: {
        'Content-Type': 'application/json',
      },
    }).then((resp) => {
      // const mySession = session;
      var response = JSON.parse(aesDecrypt(resp.data.data));
      console.log("================response    token---", response);
      sessionStorage.setItem("session", response.session);
      mySession.connect(response.token, "agent")
        .then(() => {
          let publisher = OV.initPublisher(undefined, {
            audioSource: undefined,
            videoSource: undefined,
            publishAudio: true,
            publishVideo: true,
            // resolution: '640x480',
            resolution: '800x600',
            frameRate: 30,
            insertMode: 'APPEND',
            mirror: false,
          });
          sessionStorage.setItem("publisher", Object.keys(publisher));
          mySession.publish(publisher);
          // $this.setState({
          //   mainStreamManager: publisher,
          //   publisher: publisher,
          // });
          setMainStreamManager(publisher);
          setPublisher(publisher);
          
          // createRecording(sessionId, mySession, ovData);
        })
        .catch((error) => {
          console.log('There was an error connecting to the session:', error.code, error.message);
        });
      // dispatch({
      //   type: actionTypes.CHAT_TOKEN,
      //   payload: response.token
      // });
    }).catch((error) => {
      console.log(error);
    })
  }


  const createRecording = (sessionId, mySession, ovData) => {
    var data = {
      session: sessionId
    };
    let body = {
      "data": aesEncrypt(data)
    }
    instance.post('/tokens', body, {
      headers: {
        'Content-Type': 'application/json',
      },
    }).then((resp) => {
      var response = JSON.parse(aesDecrypt(resp.data.data));
      sessionStorage.setItem("session", response.session)
      let OV2 = new OpenVidu();
      let mySession2 = OV2.initSession();
      mySession2.connect(response.token, "screen")
        .then(() => {
          let publisher2 = OV2.initPublisher(undefined, {
            audioSource: undefined,
            videoSource: 'screen',
            publishAudio: true,
            publishVideo: true,
            // resolution: '640x480',
            frameRate: 30,
            insertMode: 'APPEND',
            mirror: false,
          });
          mySession2.publish(publisher2);
          setSession2(mySession2);
          createTokenAction(sessionId, mySession, ovData);
        })
        .catch((error) => {
          console.log('There was an error connecting to the session:', error.code, error.message);
        });
    }).catch((error) => {
      console.log(error);
    })
  }
  // DELETE VIDEO CALL SUBSCRIBER
  const deleteSubscriber = (streamManager) => {
    let subscribersData = subscribers;
    let index = subscribersData?.indexOf(streamManager, 0);
    if (index > -1) {
      subscribersData?.splice(index, 1);
      setSubscribers(subscribersData);
    }
  }

  // END VIDEO CALL
  const leaveSession = () => {
    // const session = session;
    if (mySession) {
      mySession.disconnect();
      toast.error("Customer Disconnected");
    }
    if (session2) {
      session2.disconnect();
      toast.error("Customer Disconnected");
    }
    OV = null;
    // setOV(null)
    setSession(undefined);
    setSubscribers([]);
    setMainStreamManager(undefined);
    setPublisher(undefined)
    const model = {
      notificationid: "2",
      vcipkey: sessionStorage.getItem('vcipkey'),
      notifymsg: "Call End"
    }
    dispatch(endVideoCallSagaAction({ callback: getEndCallRespData }));
    dispatch(pushNotificationSaga({ model: model, callback: pushNotificationRespData }));
    sessionStorage.removeItem("connectionId");
    sessionStorage.removeItem("publisher");
    sessionStorage.removeItem("session");
  }

  const getEndCallRespData = (data) => {
    navigate('/dashboard/live', { replace: true });
    // const model = {
    //   vcipkey: sessionStorage.getItem('vcipkey'),
    //   vcipstatus: "3", // 2 - Approved, 3- Rejected
    //   agentstatus: "3", // 1 - Approved, 2 - Rejected  3-  issued
    //   remarks: ""
    // }
    // dispatch(updateVcipStatusSagaAction({ model: model, callback: updateVcipStatusRes }));
  }

  const updateVcipStatusRes = () => {
    console.log("===========>");
    navigate('/dashboard/live', { replace: true });
  }

  const pushNotificationRespData = (data) => {
    sessionStorage.removeItem("connectionId");
    sessionStorage.removeItem("publisher");
    sessionStorage.removeItem("session");
    endVideoCall();
    // stopScreenRecord();
    console.log("session2===comp", session2);
  }

  // END VIDEO CALL
  const endVideoCall = () => {
    if (mySession) {
      mySession.disconnect();
    }
    sessionStorage.removeItem("session");
    sessionStorage.removeItem("connectionId");
    // sessionStorage.removeItem("videoconfsessionid");
    sessionStorage.removeItem("publisher");
    sessionStorage.removeItem("session");
    // setTimeout(() => {
    // navigate('/dashboard/live', { replace: true });
    // }, 1000);
  }

  // END CALL WITHOUT CLICKING END BUTTON
  // const leaveCallWithoutEndSession = () => {
  //   // const mySession = this.state.session;
  //   if (session) {
  //     session.disconnect();
  //   }
  //   // OV = null;

  //   // setOV(undefined);
  //   setSubscribers([]);
  //   setMainStreamManager(undefined);
  //   setSession(undefined);
  //   sessionStorage.removeItem("connectionId");
  //   sessionStorage.removeItem("publisher");
  //   sessionStorage.removeItem("session");
  // }

  // // END VIDEO CALL
  // const leaveSession = () => {
  //   // clearInterval(this.state.intervalId1);
  //   clearNotificationInterval();
  //   // const mySession = this.state.session;
  //   if (session) {
  //     session.disconnect();
  //     // toast.error("Customer Disconnected");
  //   }
  //   // OV = null;
  //   // setOV(undefined);
  //   setSubscribers([]);
  //   setMainStreamManager(undefined);
  //   setSession(undefined);
  //   const model = {
  //     notificationid: "2",
  //     vcipkey: sessionStorage.getItem('vcipkey'),
  //     notifymsg: "Call End"
  //   }
  //   dispatch(endVideoCallSagaAction({ callback: getEndCallRespData }));
  //   dispatch(pushNotificationSaga({ model: model, callback: pushNotificationRespData }));
  //   sessionStorage.removeItem("connectionId");
  //   sessionStorage.removeItem("publisher");
  //   sessionStorage.removeItem("session");
  // }


  console.log("op->->subscribers", subscribers, "op->->mainStreamManager", mainStreamManager, "op->->session", mySession);

  return (
    <>
      {!mySession
        ? <InitiatingVideoCallCmp />
        : <>
          {subscribers && subscribers.length !== 0 ? null
            : <p className="text-center text-danger position-absolute">
              not Available
            </p>
          }

          <div className="row m-0 flex-1">
            <div className="col-6 ps-0">
              <div className="vd-bx">
                <div className="vd-bx-agnt">
                  <span className='vd-name'>Agent</span>
                  {mainStreamManager ? (
                    <div id="cusomer" className='h-100'>
                      <OpenViduVideoComponent streamManager={mainStreamManager} />
                    </div>
                  ) : null}
                </div>
              </div>
              <button className='btn' onClick={leaveSession}>End Call</button>
            </div>
            <div className="col-6">
              <div className="vd-bx h-100">
                <div className="vd-bx-agnt h-100">
                  <span className='vd-name'>Customer</span>
                  {subscribers?.map((sub, i) => {
                    if (sub.stream.typeOfVideo !== "SCREEN") {
                      // return <div key={i} className="stream-container othervideo cstmr-vd-call h-100">
                      return <div key={i} className="stream-container othervideo cstmr-vd-call h-100">
                        <VideoSession streamManager={sub} />
                      </div>
                    }
                  })}
                </div>
              </div>
            </div>
          </div>
        </>
      }
    </>
  )
}

export default InitiatingVideoCall;