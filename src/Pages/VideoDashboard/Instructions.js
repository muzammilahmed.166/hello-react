import React, { useContext, useState } from 'react'
import { VCIPDetailsContext } from './VideoDashboard'

const Instructions = () => {

  const { customerVcipDetails, navigate, vcipid } = useContext(VCIPDetailsContext);
  const [isLoading, setIsLoading] = useState(false);


  return (
    <>
      <article className='prt-1'>
        <h5>Please verify the below instructions to begin</h5>
        <hr className="hr-horizontal" />
        <ul className='intrctn-list'>
          <li className="before-icon">User is alone on the call (unless BC assited)</li>
          <li className="before-icon">User face can be seen clearly</li>
          <li className="before-icon">User voice can be heard clearly</li>
          <li className="before-icon">User can see you clearly</li>
          <li className="before-icon">User can hear you clearly</li>
        </ul>
      </article>
      <article className='prt-2'>
        <div className="video-options">
          <div className="sub-video-options">
            <div className='sub-video-options-img'>
              <img src="../images/Vector-mic.svg" alt="mic" />
            </div>
            <h3>Enabled</h3>
            <h6>Microphone</h6>
          </div>
          <div className="sub-video-options">
            <div className='sub-video-options-img'>
              <img src="../images/Vector-video.svg" alt="video" />
            </div>
            <h3>Enabled</h3>
            <h6>Camera</h6>
          </div>
          <div className="sub-video-options">
            <div className='sub-video-options-img'>
              <img src="../images/Vector-download-arrow.svg" alt="download" />
            </div>
            <h3>{customerVcipDetails?.customerdetails?.nw_incoming} Mbps</h3>
            <h6>Download speed</h6>
          </div>
          <div className="sub-video-options">
            <div className='sub-video-options-img'>
              <img src="../images/Vector-upload-arrow.svg" alt="upload" />
            </div>
            <h3>{customerVcipDetails?.customerdetails?.nw_outgoing} Mbps</h3>
            <h6>Upload speed</h6>
          </div>
          <div className="sub-video-options">
            <div className='sub-video-options-img'>
              <img src="../images/screen.svg" alt="screen" />
            </div>
            <h3>{customerVcipDetails?.customerdetails?.videoresolution}</h3>
            <h6>Video resolution</h6>
          </div>
        </div>
        <button className="btn" onClick={() => navigate(`/video/${vcipid}/questions`)} disabled={isLoading}>Next {isLoading ? <span className="spinner">{console.log(isLoading, "isLoading")}</span> : console.log("isloding")}</button>
      </article>
    </>
  )
}

export default Instructions