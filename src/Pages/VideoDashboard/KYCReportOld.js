import React, { useContext } from 'react'
import KYCReportCmp from '../../Components/VideoCallCmp/KYCReportCmp'
import { VCIPDetailsContext } from './VideoDashboard';
import './report.css';

const KYCReport = () => {
    const { customerVcipDetails, navigate, vcipid, closeReport } = useContext(VCIPDetailsContext);
    return (
        <>
            <KYCReportCmp
                customerVcipDetails={customerVcipDetails}
                closeReport={closeReport}
            />
        </>
    )
}

export default KYCReport