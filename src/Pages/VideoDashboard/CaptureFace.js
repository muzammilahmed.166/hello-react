import React, { useContext, useState } from 'react'
import { useDispatch } from 'react-redux';
import CaptureFaceCmp from '../../Components/VideoCallCmp/CaptureFaceCmp'
import { statusbarUpdateSagaAction } from '../../Store/SagaActions/CommonSagaActions';
import { livenessCheckSagaAction } from '../../Store/SagaActions/KYCProcessSagaAction';
import { VCIPDetailsContext } from './VideoDashboard';

const CaptureFace = () => {
  const { customerVcipDetails, navigate, vcipid, img, captureCustomerImg } = useContext(VCIPDetailsContext);
  const [isCaptured, setIsCaptured] = useState(false);
  const [isConfirmed, setIsConfirmed] = useState(false);
  const [livenessData, setLivenessData] = useState({});
  const [isLoading, setIsLoading] = useState(false);

  const dispatch = useDispatch();

  const capture = () => {
    captureCustomerImg();
    setIsCaptured(true);
    setIsConfirmed(false);

  }

  const retake = () => {
    setIsCaptured(false);
    setIsConfirmed(false);

  }

  // const confirm = () => {
  //   // setIsCaptured(true);
  //   liveCheck();
  // }

  const liveCheck = () => {
    const base64Img = img?.split(',')[1]
    const model = {
      vcipkey: sessionStorage.getItem('vcipkey'),
      liveimage: base64Img,
      rrn: 1,
    }
    dispatch(livenessCheckSagaAction({ model: model, callback: getMatchData }))
  }

  const getMatchData = (data) => {
    setIsConfirmed(true)
    setIsLoading(true);

    setLivenessData(data);
  }

  const nextPage = () => {
    dispatch(statusbarUpdateSagaAction({ captureFace: true }));
    setIsLoading(true);
    navigate(`/video/${vcipid}/aadhar-check`)
  }

  return (
    <>
      <CaptureFaceCmp
        img={img}
        isCaptured={isCaptured}
        isConfirmed={isConfirmed}
        livenessData={livenessData}
        capture={capture}
        retake={retake}
        confirm={liveCheck}
        nextPage={nextPage}
        isLoading = {isLoading}

      />
    </>
  )
}

export default CaptureFace