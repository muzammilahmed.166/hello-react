import React, { useContext, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import AadharCheckCmp from '../../Components/VideoCallCmp/AadharCheckCmp';
import { faceMatchSagaAction, updateMatchStatusByAgentSagaAction } from '../../Store/SagaActions/KYCProcessSagaAction';
import { VCIPDetailsContext } from './VideoDashboard';

const AadharCheck = () => {
  const { customerVcipDetails, navigate, vcipid, img } = useContext(VCIPDetailsContext);

  const [isMatchChecked, setIsMatchChecked] = useState(false);
  const [facematchDetails, setFacematchDetails] = useState({});
  const [isSelected, setIsSelected] = useState('');
  const  [isLoading, setIsLoading] = useState(false);
  const [disabledBtn, setDisabledBtn] = useState(true);

  const dispatch = useDispatch();
  // const aadhaarOfflineMatchData = useSelector(state => state?.VcipReducer?.aadhaarOfflineMatchData);

  const vcipkey = sessionStorage.getItem('vcipkey');

  useEffect(() => {
    const base64Img = img?.split(',')[1];
    const model = {
      vcipkey: vcipkey,
      image1: customerVcipDetails?.kycdetails?.pht,
      image2: base64Img,
      matchtype: '2', //2-  kyc live
      rrn: '1'
    }
    dispatch(faceMatchSagaAction({ model: model, callback: getFaceMatchDetails }))
  }, []);

  const getFaceMatchDetails = (data) => {
    // setIsLoading(true);
    setFacematchDetails(data);
  }

  const updateFacematchStatus = (val) => {
    const model = {
      vcipkey: vcipkey,
      matchtype: '1', //1-  kyc live
      matchstatus: val
    }
    setDisabledBtn(!disabledBtn)

    dispatch(updateMatchStatusByAgentSagaAction({ model: model, callback: updateFaceMatchRes }))
  }

  const updateFaceMatchRes = (data, type) => {
    setIsLoading(true);
    if (data?.respcode === "200") {
      setIsLoading(false);

      setIsMatchChecked(true);
      setIsSelected(type);      
    } else {
      setIsMatchChecked(false);
      setIsSelected('');   
    }
  }


  const matchAadharWithLive = () => {
    setIsMatchChecked(true);
  }

  return (
    <>
      <AadharCheckCmp
        facematchDetails={facematchDetails}
        customerVcipDetails={customerVcipDetails}
        img={img}
        isMatchChecked={isMatchChecked}
        isSelected={isSelected}
        // isLoading = {isLoading}
        updateFacematchStatus={updateFacematchStatus}
        disabledBtn={disabledBtn}
        // aadhaarOfflineMatchData={aadhaarOfflineMatchData}
        nextPage={() => navigate(`/video/${vcipid}/aadhar-details-check`)}
      />
    </>
  )
}

export default AadharCheck;