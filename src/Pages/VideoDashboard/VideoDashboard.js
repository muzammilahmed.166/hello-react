import React, { useEffect, useMemo, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Outlet, useLocation, useNavigate } from 'react-router-dom';
import RecordRTC, { invokeSaveAsDialog } from "recordrtc";
import axios from 'axios';
import VideoCallHeaderCmp from '../../Components/VideoCallCmp/VideoCallHeaderCmp';
import PortalModal from '../../Portals/PortalModal';
import { actionGetVcipDetails } from '../../Store/SagaActions/GetVcipDetailsSagaActions';
import { endVideoCallSagaAction, updateVcipStatusSagaAction } from '../../Store/SagaActions/VideoSagaActions';
import InitiatingVideoCall from './InitiatingVideoCall/InitiatingVideoCall';
// import InitiatingVideoCall from './InitiatingVideoCall/InitiatingVideoCallNew';
import IssueModal from './InitiatingVideoCall/Modals/IssueModal';
import KYCReport from './KYCReport';
// import './statusbar.css';
// import FileUploadProgress from 'react-fileupload-progress';
import { uploadScreenRecordVideoSagaAction } from '../../Store/SagaActions/KYCProcessSagaAction';
import { actionReqResStatusLoaderSagaAction, endVideoCallByAgentSagaAction, pushNotificationSaga } from '../../Store/SagaActions/CommonSagaActions';
import Header from '../../Pages/Common/Header';
import LanguageModal from '../Modals/LanguageModal';



export const VCIPDetailsContext = React.createContext();


var isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);

const VideoDashboard = () => {

    const [customerVcipDetails, setCustomerVcipDetails] = useState({});
    const [img, setImg] = useState('');
    const [isReportOpened, setIsReportOpened] = useState(false);
    const [isIssueOpened, setIsIssueOpened] = useState(false);
    const [lngModalOpen, setLngModalOpen] = useState(false);


    // screen record
    // var recorder;
    const [startDisable, setStartDisable] = useState(false);
    const [stopDisable, setStopDisable] = useState(true);
    const [screen, setScreen] = useState(null);
    const [camera, setCamera] = useState(null);
    const [recordedVideoUrl, setRecordedVideoUrl] = useState('');
    const [videodispay, setVideodispay] = useState(false);
    const [recorder, setRecorder] = useState({})

    const dispatch = useDispatch();
    const navigate = useNavigate();
    const location = useLocation();

    const isLoading = useSelector(state => state.HomeReducer.apiStatus);
    const isCallEnded = useSelector(state => state.HomeReducer.isCallEnded);
    const vcipkey = sessionStorage.getItem('vcipkey');

    useEffect(() => {
        startRecordvideo();
        getVcipIdDetailsById(false);
        if (location?.pathname === `/video/${customerVcipDetails?.vcipid}/KYC-Report`) {
            setIsReportOpened(true);
        }
    }, []);

    useEffect(() => {
        closeLangModal();
    }, [])
    
    const closeLangModal = () => setLngModalOpen(!lngModalOpen);

    const getVcipIdDetailsById = (reportFlag) => {
        const model = {
            vcipkey: sessionStorage.getItem('vcipkey'),
        }
        dispatch(actionGetVcipDetails({ model: model, reportFlag: reportFlag, callback: getVcipDetailsData }));
    }

    const getVcipDetailsData = (data, reportFlag) => {
        setCustomerVcipDetails(data);
        if (reportFlag) {
            setIsReportOpened(true);
            // dispatch(endVideoCallByAgentSagaAction(false));
            navigate(`/video/${data?.vcipid}/KYC-Report`)
        }
    }

    useEffect(() => {
        const unloadCallback = (event) => {
            event.preventDefault();
            event.returnValue = "";
            return "";
        };

        window.addEventListener("beforeunload", unloadCallback);
        return () => window.removeEventListener("beforeunload", unloadCallback);
    }, []);


    const startRecordvideo = () => {
        setVideodispay(true);
        // this.setState({
        //     videodispay: true,
        // });
        startScreenRecord();
        if (sessionStorage.reload) {
            sessionStorage.reload = true;
            // optionnal
            setTimeout(() => { sessionStorage.setItem('reload', false) }, 2000);
        } else {
            sessionStorage.setItem('reload', false);
        }
        if (sessionStorage.getItem('reloaded') != null) {
            // this.props.urlPath.replace("/");
            // window.location.reload(true);
        } else {
            console.log('page was not reloaded');
        }

        sessionStorage.setItem('reloaded', 'yes');
    };

    //access your screen width and height  using window object adjusting camera position ,height and width  //after that pass screen and camera to recordrtc/and call startrecording method using recorder object to //start screen recording
    const startScreenRecord = async () => {
        setStopDisable(false);
        setStartDisable(false);
        captureScreen((screen) => {
            captureCamera(async (camera) => {
                screen.width = window.screen.width;
                screen.height = window.screen.height;
                screen.fullcanvas = true;
                camera.width = 320;
                camera.height = 240;
                camera.top = screen.height - camera.height;
                camera.left = screen.width - camera.width;
                setScreen(screen);
                setCamera(camera);
                // this.setState({
                //     screen: screen,
                //     camera: camera,
                // });
                let recorder = RecordRTC([screen, camera], {
                    type: "video",
                });

                recorder.startRecording();
                recorder.screen = screen;
                setRecorder(recorder)
                // this.props.GetQuestionsAction();
                // this.refreshpage();
            });
        });
    };

    //to enable audio and video pass true to disable pass false
    const captureCamera = (cb) => {
        navigator.mediaDevices
            .getUserMedia({
                audio: true,
                video: false, //make it true for video
            })
            .then(cb);
    };

    //to capture screen  we need to make sure that which media devices are captured and add listeners to // start and stop stream
    const captureScreen = (callback) => {
        setTimeout(() => {
            try {
                invokeGetDisplayMedia(
                    (screen) => {
                        addStreamStopListener(screen, () => { });
                        callback(screen);
                    },
                    (error) => {
                        console.error(error);
                        alert(
                            "Please click share to continue"
                            // "Unable to capture your screen. Please check console logs.\n" + error
                        );
                        window.location.reload();
                        setStopDisable(true);
                        setStartDisable(true);
                        // this.setState({ stopDisable: true, startDisable: false });
                    }
                );
            } catch (e) {
                console.log(e, "error");
            }

        }, 1000);
    };

    //tracks stop
    const stopLocalVideo = async (screen, camera) => {
        [screen, camera].forEach(async (stream) => {
            stream.getTracks().forEach(async (track) => {
                track.stop();
            });
        });
    };

    //getting media items
    const invokeGetDisplayMedia = (success, error) => {
        var displaymediastreamconstraints = {
            video: {
                displaySurface: "monitor", // monitor, window, application, browser
                logicalSurface: true,
                cursor: "always", // never, always, motion
            },
        };
        displaymediastreamconstraints = {
            video: true,
            audio: true,
        };
        if (navigator.mediaDevices.getDisplayMedia) {
            try {
                navigator.mediaDevices
                    .getDisplayMedia(displaymediastreamconstraints)
                    .then(success)
                    .catch(error);
            } catch (e) {
                console.log(e, "error");
            }
        } else {
            try {
                navigator
                    .getDisplayMedia(displaymediastreamconstraints)
                    .then(success)
                    .catch(error);
            } catch (e) {
                console.log(e, "error");
            }
        }
    };

    const addStreamStopListener = (stream, callback) => {
        stream.addEventListener(
            "ended",
            () => {
                callback();
                callback = () => { };
            },
            false
        );
        stream.addEventListener(
            "inactive",
            () => {
                callback();
                callback = () => { };
            },
            false
        );
        stream.getTracks().forEach((track) => {
            track.addEventListener(
                "ended",
                () => {
                    callback();
                    callback = () => { };
                },
                false
            );
            track.addEventListener(
                "inactive",
                () => {
                    callback();
                    callback = () => { };
                },
                false
            );
        });
        stream.getVideoTracks()[0].onended = () => {
            stop();
        };
    };

    // stop screen recording
    const stop = async () => {
        try {
            setStartDisable(true);
            // await this.setState({ startDisable: true, stopstyling: true });
            recorder.stopRecording(stopRecordingCallback);

            // this.stopTimer();
            // if (window.timeout) {
            //     // $('#faceMatchliveness').modal('show');

            //     // $('#modaldispay').modal('show');
            //     setTimeout(() => {
            //         // $('#faceMatchliveness').modal('hide');
            //     }, 3000);
            // }
        } catch (e) {
            console.log("error");
            // console.log(e)
        }
    };

    //destory screen recording
    const stopRecordingCallback = async () => {
        await stopLocalVideo(screen, camera);
        let recordedVideoUrl;

        if (recorder.getBlob()) {
            // this.setState({
            //     recordPreview: recorder.getBlob(),
            // });

            // RecordRTC.bytesToSize = bytesToSize;
            // let size = bytesToSize(recorder.getBlob());
            // console.log(size)

            recordedVideoUrl = URL.createObjectURL(recorder.getBlob());
            console.log(recordedVideoUrl, 'link')
        }
        // this.setState({
        //     recordedVideoUrl: recordedVideoUrl,
        //     screen: null,
        //     isOpenVideoModal: true,
        //     startDisable: false,
        //     stopDisable: true,
        //     camera: null,
        // });
        if (!recordedVideoUrl) {
            return;
        }
        if (isSafari) {
            if (recordedVideoUrl && recordedVideoUrl.getDataURL) {
                recordedVideoUrl.getDataURL(function (dataURL) {
                    RecordRTC.SaveToDisk(dataURL, "filename.mp4");

                });
                return;
            }
        }
        // const $this = this;

        if (recordedVideoUrl) {
            // var blob = recorder.getBlob();
            var blob = await recorder.getBlob();
            var file = new File([blob], getFileName("mp4"), {
                type: "video/mp4",
            });
            console.log("================file", file);

            UploadVCIPRecordedVideo(file);
            // dispatch(uploadScreenRecordVideoSagaAction({ model: formData }))
        }
        setRecordedVideoUrl(recordedVideoUrl);
        setScreen(null);
        setStartDisable(false);
        setStopDisable(true);
        setCamera(null);
        recorder.screen.stop();
        recorder.destroy();
        // recorder = null;
        setRecorder(null);
        return
    };
    const getFileName = (fileExtension) => {
        var d = new Date();
        var year = d.getFullYear();
        var month = d.getMonth();
        var date = d.getDate();
        return (
            "ScreenRecord-" +
            year +
            month +
            date +
            "-" +
            "Video" +
            "." +
            fileExtension
        );
    };

    const UploadVCIPRecordedVideo = (file) => {
        const URL = process.env.REACT_APP_BASE_URL + "UploadVCIPRecordedVideo";
        const authkey = sessionStorage.getItem('authkey');
        // request.headers.common["authkey"] = authkey;
        let formData = new FormData();
        formData.append('vcipkey', vcipkey);
        formData.append('ftype', '.mp4');
        // console.log(file)
        dispatch(actionReqResStatusLoaderSagaAction(true))
        formData.append(
            "myFile",
            file
        );
        axios({
            method: "POST",
            url: URL,
            data: formData,
            headers: {
                "Content-Type": "multipart/form-data",
                "authkey": authkey,
                "apikey": "0",
            },
        })
            .then(response => {
                // $this.props.urlPath.push('/end');
                dispatch(actionReqResStatusLoaderSagaAction(false));
                navigate('/dashboard/live', { replace: true });
            })
            .catch(error => {
                console.log(error);
                dispatch(actionReqResStatusLoaderSagaAction(false));
            });
    }


    const captureCustomerImg = () => {
        try {
            var video = document.querySelector(".streamcomponent .video-play"); // FOR CUSTOMER PIC
            // var video = document.querySelector("#cusomer .video-play"); // FOR AGENT PIC
            var canvas = document.getElementById('canvas');
            canvas.width = video.videoWidth;
            canvas.height = video.videoHeight;
            // canvas.getContext('2d').drawImage(video, 0, 0, 450, 850);
            canvas.getContext('2d').drawImage(video, 0, 0, video.videoWidth, video.videoHeight);
            var data = canvas.toDataURL('image/png');
            setImg(data);
            // console.log(data);


        } catch (e) {
            console.log(e, 'try catch error')
        }
    }

    const openReport = () => {
        // getVcipIdDetailsById(true)
        setIsReportOpened(prevState => !prevState);
    }

    const closeReport = () => {
        setIsReportOpened(false);
    }

    const openIssueModal = () => {
        setIsIssueOpened(prevState => !prevState);
    }

    // ISSUE MODAL CALLING
    const endVcipCallWithIssue = (remarks) => {
        updateVCIPStatusByAgent("3", "3", remarks);
    }

    // FINAL REPORT CALLING
    const updateKYCReportFinalStatus = (status, remarks) => {
        const agentstatus = status === "2" ? "1" : "2";
        updateVCIPStatusByAgent(status, agentstatus, remarks);
    }

    const updateVCIPStatusByAgent = (vcipstatus, agentstatus, remarks) => {
        const model = {
            vcipkey: sessionStorage.getItem('vcipkey'),
            vcipstatus: vcipstatus, // 2 - Approved, 3- Rejected
            agentstatus: agentstatus, // 1 - Approved, 2 - Rejected  3-  issued
            remarks: remarks
        }
        dispatch(updateVcipStatusSagaAction({ model: model, callback: updateVcipStatusRes }));
    }

    const updateVcipStatusRes = () => {
        dispatch(endVideoCallByAgentSagaAction(true));
        stop();
        dispatch(endVideoCallSagaAction({ callback: getEndCallRespData }));
        // setTimeout(() => {
        //     navigate('/dashboard/live', { replace: true });
        // }, 2000);
    }

    const endCustomerAgentVideo = () => {
        dispatch(endVideoCallSagaAction({ callback: getEndCallRespData }));
        // dispatch(endVideoCallByAgentSagaAction(true));
        // getVcipIdDetailsById(true);
    }

    const getEndCallRespData = (data) => {
        pushNotificationForEnd();
        // const model = {
        //   vcipkey: sessionStorage.getItem('vcipkey'),
        //   vcipstatus: "3", // 2 - Approved, 3- Rejected
        //   agentstatus: "3", // 1 - Approved, 2 - Rejected  3-  issued
        //   remarks: ""
        // }
        // stop();
        // dispatch(updateVcipStatusSagaAction({ model: model, callback: updateVcipStatusRes }));
    }
    const pushNotificationForEnd = () => {
        const model = {
            notificationid: "2", // 2 - END CALL
            vcipkey: sessionStorage.getItem('vcipkey'),
            notifymsg: "Call End"
        }
        dispatch(pushNotificationSaga({ model: model, callback: pushNotificationRespData }));

    }

    const pushNotificationRespData = (data) => {
        dispatch(endVideoCallByAgentSagaAction(true));
        getVcipIdDetailsById(true);
    }

    const closeModal = () => {
        setIsIssueOpened(prevState => !prevState)
    }

    const initiatingVideo = useMemo(() => (!isReportOpened && <InitiatingVideoCall />), [customerVcipDetails]);

    return (
        <>
            {/* <div>
                <h3>Default style</h3>
                <FileUploadProgress key='ex1' url='http://localhost:3000/api/upload'
                    onProgress={(e, request, progress) => { console.log('progress', e, request, progress); }}
                    onLoad={(e, request) => { console.log('load', e, request); }}
                    onError={(e, request) => { console.log('error', e, request); }}
                    onAbort={(e, request) => { console.log('abort', e, request); }}
                />
            </div> */}
            <Header />
            <VCIPDetailsContext.Provider value={{
                customerVcipDetails: customerVcipDetails,
                vcipid: customerVcipDetails?.vcipid,
                img: img,
                navigate: navigate,
                captureCustomerImg: captureCustomerImg,
                openIssueModal: openIssueModal,
                stop: stop,
                openReport: openReport,
                closeReport: closeReport,
                endCustomerAgentVideo: endCustomerAgentVideo,
                updateKYCReportFinalStatus: updateKYCReportFinalStatus
            }}>
                <VideoCallHeaderCmp />
                <div className="main-screen">
                    <div className='main-screen-btns-r'>
                        <button className='main-screen-btn'>
                            {/* <img src='../../images/icon-message.svg' alt='' /> */}
                            <i class="fa-solid fa-comment-dots fa-xl fa-flip-horizontal text-white"></i>
                        </button>
                        <button className='main-screen-btn' onClick={openIssueModal}>
                            <i class="fa-regular fa-memo fa-xl fa-flip-vertical text-white"></i>
                        </button>
                        {/* <button className='main-screen-btn' onClick={openReport}>
                            <img src='../../images/icon-report.svg' alt='' />
                        </button> */}
                    </div>
                    {/* {isReportOpened
                        && <KYCReport />} */}
                    <div className="main-screen-bx d-flex">
                        <div className="row m-0 flex-1">
                            {!isReportOpened && <div className="col-md-5 ps-0 d-flex">
                                {/* <InitiatingVideoCall /> */}
                                {initiatingVideo}
                            </div>}
                            <div className={isReportOpened ? 'col-md' : 'col-md-7 p-0'}>
                                <div className="screen3">
                                    {isLoading !== 0 && <div className='pageloader'></div>}
                                    <Outlet />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </VCIPDetailsContext.Provider>
            {isIssueOpened && <PortalModal isOpen={isIssueOpened}>
                <IssueModal endVcipCallWithIssue={endVcipCallWithIssue} closeModal={closeModal} />
            </PortalModal>}

            {lngModalOpen && <PortalModal isOpen={lngModalOpen}>
                <LanguageModal closeModal={closeLangModal} />
            </PortalModal>}
        </>
    )
}

export default VideoDashboard;