import React from 'react'

const AadharOfflineCheck = () => {
  return (
    <>
      <article className='prt-1'>
        <h5>Check liveliness</h5>
        <hr className="hr-horizontal" />
        <div className="row">
          <div className="col-md-6">
            <div className="aadhaar-offline-kyc">
              <img src="../../images/girl_rectangle.svg" alt="" />
            </div>
          </div>
          <div className="col-md-6">
            <div className="aadhaar-offline-kyc">
              <img src="../../images/girl_rectangle.svg" alt="" />
            </div>
          </div>
        </div>
        <div className="match-score mt-4">
          <h5>Match score - 99.9%</h5>
        </div>
        <div className="match-score">
          <h5>Does the face match with aadhaar photo?</h5>

          <button className="btn-round">
            <img src='../../images/icon-wrong.svg' alt="check" />
          </button>
          <button className="btn-round btn-green">
            <img src='../../images/icon-check.svg' alt="check" />
          </button>
        </div>

        {/* <div className="frm-grp mb-4">
          <input
            type="text"
            name=""
            className='frm-grp-inp'
            placeholder='Add remarks (optional)'
          />
        </div> */}

      </article>
      <article className='prt-2'>
        <button className="btn">Next</button>
      </article>
    </>
  )
}

export default AadharOfflineCheck;