import React from 'react';
import PropTypes from 'prop-types';

const AppFooter = (props) => {
    return (
        <>
            {props.isFooterAvailable ? (null) :
                <section className="app-end">
                    <div className="text-center">
                        {props?.isGenerate
                            ? <>
                                <h4 className="title">Don't have an Aadhaar XML file?</h4>
                                <button className="btn btn-white mb-3" type="button" data-toggle="modal" data-target="#verification" onClick={props.generateXMLModal}>Generate now</button>
                            </>
                            : (null)
                        }
                        {props?.isJoinCall
                            ? <>
                                {/* <button className="btn mb-3" type="button" data-toggle="modal" data-target="#verification" onClick={props.generateXMLModal}>Join Now</button> */}
                                <button
                                    className="btn mb-3"
                                    type="button"
                                    onClick={props?.joinVideoSession}>
                                    Join Now
                                </button>
                            </>
                            : (null)
                        }
                        <button
                            className="btn"
                            type="button"
                            onClick={props?.navigate}
                            disabled={props?.isDisabled ? props?.isDisabled : false}
                        >
                            {props?.btnName ? props?.btnName : 'Continue'}
                        </button>
                    </div>
                </section>
            }
            {/* <section className="app-end">
                <div className="text-center">
                    <h4 className="title">Don't have an Aadhaar XML file?</h4>
                    <button className="btn btn-white mb-3" type="button" data-toggle="modal" data-target="#verification">Generate now</button>
                    <button className="btn" type="button" data-toggle="modal" data-target="#upload">Proceed</button>
                </div>
            </section> */}
        </>
    )
}

AppFooter.propTypes = {
    isFooterAvailable: PropTypes.bool,
    isDisabled: PropTypes.bool,
    isJoinCall: PropTypes.bool,
    isGenerate: PropTypes.bool,
    navigate: PropTypes.func,
    btnName: PropTypes.string,
    generateXMLModal: PropTypes.func,
}

export default AppFooter;