import React, { useState } from 'react'
import PanDetailsEditCmp from '../../../Components/PanCmp/PanDetailsEditCmp'

const PanDetailsEdit = (props) => {
    const [edtname, setEdtname] = useState("");
    const [edtfname, setEdtfname] = useState("");
    const [edtdob, setEdtdob] = useState("");
    const [edtpannumber, setEdtpannumber] = useState("");

    const handleChange = (e) => {
        const name = e.target.name;
        const value = e.target.value;

        switch (name) {
            case 'edtname':
                setEdtname(value);
                break;
            case 'edtfname':
                setEdtfname(value);
                break;
            case 'edtdob':
                setEdtdob(value);
                break;
            case 'edtpannumber':
                setEdtpannumber(value);
                break;

            default:
                break;
        }
    }

    const submitEditPanEdtails = () => {
        props.savePanDetails(edtname, edtfname, edtdob, edtpannumber);
    }



    return (
        <>
            <PanDetailsEditCmp
                panDetails={props?.panDetails}
                dateDisplay={props?.dateDisplay}

                handleChange={handleChange}
                submitEditPanEdtails={submitEditPanEdtails}
            />
        </>
    )
}

export default PanDetailsEdit;