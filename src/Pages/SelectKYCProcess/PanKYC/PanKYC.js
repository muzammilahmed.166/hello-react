import React, { useEffect, useState } from 'react';
import Camera from 'react-html5-camera-photo';
import 'react-html5-camera-photo/build/css/index.css';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import base64 from 'base-64';

import RouteNames from '../../../Constants/RouteNames';
import { actionPanCapture, actionSavePanDetails, actionVerifyPanNumber } from '../../../Store/SagaActions/PanSagaActions';
import AppFooter from '../../Common/AppFooter';
import PanDetailsEdit from './PanDetailsEdit';
import toast from 'react-hot-toast';

const PanKYC = () => {

  const [imgPath, setImgPath] = useState("");
  const [isCaptureDisabled, setisCaptureDisabled] = useState(false);
  const [panDetails, setPanDetails] = useState({});
  const [dateDisplay, setDateDisplay] = useState("");
  const [vcipkey, setVcipkey] = useState("");

  const dispatch = useDispatch();
  const navigate = useNavigate();

  useEffect(() => {
    const vcipkeyData = sessionStorage.getItem('vcipkey');
    setVcipkey(vcipkeyData);
  }, [])



  const handleTakePhoto = (dataUri) => {
    const base64result = dataUri.split(',')[1];
    setImgPath(base64result);
  }

  const submitCapturedPAN = () => {
    const model = {
      vcipkey: vcipkey,
      panimage: imgPath,
      pwd: '',
      rrn: '1'
    }
    dispatch(actionPanCapture({ model: model, callback: getPanCapturedData }));
  }

  const getPanCapturedData = (data) => {
    // setImgPath("");
    setPanDetails(data);
    setisCaptureDisabled(true);
    dateFormat(data?.pandetails?.dob)
  }


  const dateFormat = (val) => {
    var newdate = val.split("/").reverse().join("-");
    setDateDisplay(newdate);
  }


  const retake = () => {
    setImgPath("");
  }

  const savePanDetails = (edtname, edtfname, edtdob, edtpannumber) => {

    const panEncode = base64.encode(edtpannumber ? edtpannumber : panDetails?.pandetails?.pannumber);
    const model2 = {
      vcipkey: vcipkey,
      pannumber: panEncode,
      rrn: '1'
    }
    dispatch(actionVerifyPanNumber({ model: model2, callback: (data) => getVerifyPanData(data, edtname, edtfname, edtdob, edtpannumber) }))
  }

  const getVerifyPanData = (data, edtname, edtfname, edtdob, edtpannumber) => {
    if (edtname || edtfname || edtdob || edtpannumber) {
      const model = {
        vcipkey: vcipkey,
        edtname: edtname,
        edtfname: edtfname,
        edtdob: edtdob,
        edtpannumber: edtpannumber,
      }
      dispatch(actionSavePanDetails({ model: model, callback: getSaveDetailsRes }));
    } else {
      navigate(RouteNames.SELECT_VIDEOCALL_LANGAUAGE);
    }
  }

  const getSaveDetailsRes = (data) => {
    navigate(RouteNames.SELECT_VIDEOCALL_LANGAUAGE);
  }

  return (
    <>
      {(!imgPath && !isCaptureDisabled) && <Camera
        isImageMirror={false}
        onTakePhoto={(dataUri) => { handleTakePhoto(dataUri); }}
      />}
      {(imgPath && !isCaptureDisabled) ? <>
        <img src={"data:image/png;base64," + imgPath} alt="no img" />
        <div className='pan-bx-btns text-center mt-3'>
          <button className='btn w-auto retake mx-2' onClick={retake}>Retake</button>
          <button className='btn w-auto mx-2' onClick={submitCapturedPAN}>Submit</button>
        </div>
      </> : (null)}

      {isCaptureDisabled &&
        <PanDetailsEdit
          panDetails={panDetails?.pandetails}
          dateDisplay={dateDisplay}
          savePanDetails={savePanDetails}
        />}

      {/* {imgPath && <AppFooter
        btnName="Submit"
        navigate={submitCapturedPAN}
      />} */}
    </>
  )
}

export default PanKYC;