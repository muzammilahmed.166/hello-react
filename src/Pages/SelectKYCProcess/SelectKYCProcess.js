import React from 'react';
import { useNavigate } from 'react-router-dom';
import SelectKYCProcessCmp from '../../Components/SelectKYCProcessCmp';
import RouteNames from '../../Constants/RouteNames';
import AppFooter from '../Common/AppFooter';

const SelectKYCProcess = () => {

    let navigate = useNavigate();

    return (
        <>
            <SelectKYCProcessCmp />
            <AppFooter
                btnName="Start"
                navigate={() => navigate(RouteNames.AADHAR_KYC_KYC_DIGILOCCKER)}
            />
        </>
    )
}

export default SelectKYCProcess;