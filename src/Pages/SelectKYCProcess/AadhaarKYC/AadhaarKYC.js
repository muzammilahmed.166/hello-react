import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import AadhaarKYCCmp from '../../../Components/SelectKYCProcessCmp/AadhaarKYCCmp';
import RouteNames from '../../../Constants/RouteNames';
import PortalModal from '../../../Portals/PortalModal';
import AppFooter from '../../Common/AppFooter';
import UserConsentModal from '../../Modals/UserConsentModal';

const AadhaarKYC = () => {

    const [isOpen, setIsOpen] = useState(false);

    let navigate = useNavigate();

    const openModal = () => {
        setIsOpen(true);
    }

    const closeModal = () => {
        setIsOpen(false);
    }

    const nextPage = () => {
        setIsOpen(false);
        navigate(RouteNames.AADHAR_KYC_PROCESS_UPLOAD)
    }


    return (
        <>
            <AadhaarKYCCmp />
            <AppFooter
                btnName="Continue"
                navigate={openModal}
            />
            <PortalModal isOpen={isOpen}>
                <UserConsentModal agree={nextPage} closeModal={closeModal} />
            </PortalModal>
        </>
    )
}

export default AadhaarKYC;