import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import AadhaarDigiLockerCmp from '../../../Components/SelectKYCProcessCmp/AadhaarDigiLockerCmp';
// import AadhaarKYCCmp from '../../../Components/SelectKYCProcessCmp/AadhaarKYCCmp';
import RouteNames from '../../../Constants/RouteNames';
import { actionAadharDigiLocker, actionAadharDigiLockerStatus } from '../../../Store/SagaActions/AadharSagaActions';
import AppFooter from '../../Common/AppFooter';
// import AppFooter from '../../Common/AppFooter';

const AadhaarDigiLocker = () => {

    let intervalId;
    let windowOpen;
    let windowopenStatus;
    const [digiLockerObj, setdigiLockerObj] = useState({});
    const [aadharDetails, setAadharDetails] = useState({});
    const [isAadharSuccess, setIsAadharSuccess] = useState(false);

    let navigate = useNavigate();
    const dispatch = useDispatch();

    useEffect(() => {
        const vcipkey = sessionStorage.getItem('vcipkey');
        const model = {
            vcipkey: vcipkey,
            doctype: 'AADHAAR',
            rrn: '1'
        }
        dispatch(actionAadharDigiLocker({ model: model, callback: getDigiLockerData }));
        return () => {
            if (intervalId) {
                clearInterval(intervalId)
            }
            if (windowopenStatus) windowOpen.close();
        };
    }, []);

    const getDigiLockerData = (data) => {
        windowOpen = window.open(data?.url, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes");
        windowopenStatus = true;
        setdigiLockerObj(data);
        const vcipkey = sessionStorage.getItem('vcipkey');
        const model = {
            vcipkey: vcipkey,
            txnid: data.txnid,
            // txnid: "235",
            rrn: "1"
        }
        intervalId = setInterval(() => {
            dispatch(actionAadharDigiLockerStatus({ model: model, callback: getDigiLockerStatusData }));
        }, 5000);
    }

    const getDigiLockerStatusData = (data) => {
        clearInterval(intervalId);
        if (windowopenStatus) windowOpen.close()
        setAadharDetails(data);
        setIsAadharSuccess(true);

        // setInterval(() => {
        //     navigate(RouteNames.PAN_KYC_PROCESS)
        // }, 1000);
    }

    const nextPage = () => {
        // setIsOpen(false);
        navigate(RouteNames.PAN_KYC_PROCESS)
    }


    return (
        <>
            <AadhaarDigiLockerCmp
                aadharDetails={aadharDetails}
                isAadharSuccess={isAadharSuccess}
            />
            {/* <AadhaarKYCCmp /> */}
            {isAadharSuccess && <AppFooter
                btnName="Continue"
                navigate={nextPage}
            />}
        </>
    )
}

export default AadhaarDigiLocker;