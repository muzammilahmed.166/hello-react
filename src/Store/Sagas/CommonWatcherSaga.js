import toast from 'react-hot-toast';
import { put, takeLatest, call, takeEvery } from 'redux-saga/effects';
import Axios from '../../Service/axios'
import { ACTION_END_VIDEO_CALL_BY_AGENT_REQ, ACTION_STATUSBAR_UPDATE, ACTION_POST_USER_PROFILE_DATA } from '../SagaActions/actionTypes';
import { actionReqResStatusLoaderSagaAction, endVideoCallByAgentSagaAction, getstatusbarUpdateSagaAction } from '../SagaActions/CommonSagaActions';
import { ACTION_END_VIDEO_REQ, ACTION_GET_LOCATION_DETAILS_REQ, ACTION_GET_NOTIFICATIONS_LIST_REQ, ACTION_GET_NOTIFICATIONS_PUSH_REQ, ACTION_UPDATE_VCIP_STATUS_REQ } from '../SagaActions/SagaActionTypes';


function* statusbarUpdateReqSaga(action) {
    yield put(getstatusbarUpdateSagaAction(action?.payload))
}


const getLocationDetailsReq = (model) => {
    const URL = "https://api.bigdatacloud.net/data/reverse-geocode-client?latitude=" + model.lat + "&longitude=" + model.long;
    return Axios.get(URL).then(res => { return res?.data })
}

function* getLocationReqSaga(action) {
    yield put(actionReqResStatusLoaderSagaAction(true));

    try {
        const resp = yield call(getLocationDetailsReq, action?.payload?.model);
        const data =
            (resp.city ? resp.city : resp.locality)
            + ", " + resp.principalSubdivision
            + ", " + resp.countryName;

        if (action?.payload?.callback) {
            action?.payload?.callback(data);
        }
    } catch (err) {
        if (err.response) {
            toast.error(err?.response?.data?.errors?.length && err?.response?.data?.errors[0]?.message);
        } else {
            toast.error(err.message);
        }
    } finally {
        yield put(actionReqResStatusLoaderSagaAction(false));
    }
}

const getNotificationListReq = () => {
    const URL = 'GetNotifications';
    const vcipkeyData = sessionStorage.getItem('vcipkey');
    const body = {
        vcipkey: vcipkeyData
    }
    return Axios.post(URL, body).then(res => { return res?.data })
}

function* getNotificationListReqSaga(action) {
    // yield put(actionReqResStatusLoaderSagaAction(true));
    try {
        const resp = yield call(getNotificationListReq);
        if (resp?.respcode === '200') {
            if (action?.payload?.callback) {
                action?.payload?.callback(resp);
            }
        }
    } catch (err) {
        if (err.response) {
            toast.error(err?.response?.data?.errors?.length && err?.response?.data?.errors[0]?.message);
        } else {
            toast.error(err.message);
        }
    }
    //  finally {
    //     yield put(actionReqResStatusLoaderSagaAction(false));
    // }
}

const pushtNotificationReq = (model) => {
    const URL = 'PushNotifications';
    return Axios.post(URL, model).then(res => { return res?.data })
}

function* pushtNotificationReqSaga(action) {
    // yield put(actionReqResStatusLoaderSagaAction(true));
    try {
        const resp = yield call(pushtNotificationReq, action?.payload?.model);
        if (resp?.respcode === '200') {
            if (action?.payload?.callback) {
                action?.payload?.callback(resp);
            }
        }
    } catch (err) {
        if (err.response) {
            toast.error(err?.response?.data?.errors?.length && err?.response?.data?.errors[0]?.message);
        } else {
            toast.error(err.message);
        }
    }
    //  finally {
    //     yield put(actionReqResStatusLoaderSagaAction(false));
    // }
}


const endVideoCallReq = () => {
    const URL = 'EndVideoConferenceSessionID';
    const vcipkeyData = sessionStorage.getItem('vcipkey');
    const body = {
        vcipkey: vcipkeyData
    }
    return Axios.post(URL, body).then(res => { return res?.data })
}

function* endVideoCallReqSaga(action) {
    // yield put(actionReqResStatusLoaderSagaAction(true));
    try {
        const resp = yield call(endVideoCallReq);
        if (resp?.respcode === '200') {
            if (action?.payload?.callback) {
                action?.payload?.callback(resp);
            }
        }
    } catch (err) {
        if (err.response) {
            toast.error(err?.response?.data?.errors?.length && err?.response?.data?.errors[0]?.message);
        } else {
            toast.error(err.message);
        }
    }
    // sessionStorage.clear();

    //  finally {
    //     yield put(actionReqResStatusLoaderSagaAction(false));
    // }
}

const updateVCIPStatusReq = (model) => {
    const URL = 'UpdateVCIPIDStatusByAgent';
    return Axios.post(URL, model).then(res => { return res?.data })
}

function* updateVCIPStatusReqSaga(action) {
    yield put(actionReqResStatusLoaderSagaAction(true));
    try {
        const resp = yield call(updateVCIPStatusReq, action?.payload?.model);
        if (resp?.respcode === '200') {
            if (action?.payload?.callback) {
                action?.payload?.callback(resp);
            } else {
                toast.error(resp?.respdesc);
            }
        }
    } catch (err) {
        if (err.response) {
            toast.error(err?.response?.data?.errors?.length && err?.response?.data?.errors[0]?.message);
        } else {
            toast.error(err.message);
        }
    }
     finally {
        yield put(actionReqResStatusLoaderSagaAction(false));
    }
}

const getUserProfileReq = () => {
    const URL = 'UserProfile';
    return Axios.post(URL).then(res => { return res?.data })
}

function* getUserProfileReqSaga(action) {
    yield put(actionReqResStatusLoaderSagaAction(true));
    try {
        const resp = yield call(getUserProfileReq);
        if (action?.payload?.callback) {
            action?.payload?.callback(resp);
    }

        // if (resp?.respcode === '200') {
        //     if (action?.payload?.callback) {
        //         action?.payload?.callback(resp);
        //     } else {
        //         toast.error(resp?.respdesc);
        //     }
        // }
    } catch (err) {
        if (err.response) {
            toast.error(err?.response?.data?.errors?.length && err?.response?.data?.errors[0]?.message);
        } else {
            toast.error(err.message);
        }
    }
     finally {
        yield put(actionReqResStatusLoaderSagaAction(false));
    }
}

// function* endVideoCallByAgentReqSaga(action) {
//     yield put(endVideoCallByAgentSagaAction(action?.payload));
// }

export default function* CommonWatcherSaga() {
    yield takeLatest(ACTION_STATUSBAR_UPDATE, statusbarUpdateReqSaga);
    yield takeLatest(ACTION_GET_LOCATION_DETAILS_REQ, getLocationReqSaga);
    yield takeLatest(ACTION_GET_NOTIFICATIONS_LIST_REQ, getNotificationListReqSaga);
    yield takeEvery(ACTION_GET_NOTIFICATIONS_PUSH_REQ, pushtNotificationReqSaga);
    yield takeLatest(ACTION_END_VIDEO_REQ, endVideoCallReqSaga);
    yield takeLatest(ACTION_UPDATE_VCIP_STATUS_REQ, updateVCIPStatusReqSaga);
    yield takeLatest(ACTION_POST_USER_PROFILE_DATA, getUserProfileReqSaga);
    // yield takeLatest(ACTION_END_VIDEO_CALL_BY_AGENT_REQ, endVideoCallByAgentReqSaga);
}