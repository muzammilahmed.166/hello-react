import toast from 'react-hot-toast';
import { put, takeLatest, call } from 'redux-saga/effects';
import StatusCodes from '../../Constants/StatusCodes';
import Axios from '../../Service/axios'
import { actionReqResStatusLoaderSagaAction } from '../SagaActions/CommonSagaActions';
import { actionGetVcipDetailsResponse } from '../SagaActions/GetVcipDetailsSagaActions';
import { ACTION_GET_CUSTOMER_CREATE_SLOT_REQ, ACTION_GET_CUSTOMER_VCIP_DETAILS_REQ } from '../SagaActions/SagaActionTypes';

// GET VCIP DETAILS
const getVcipDetailsReq = (body) => {
    const URL = 'GetVCIPIDDetails';
    return Axios.post(URL, body).then(res => { return res?.data })
}

function* getVcipDetialsReqSaga(action) {
    yield put(actionReqResStatusLoaderSagaAction(true));

    try {
        const resp = yield call(getVcipDetailsReq, action?.payload?.model);
        if (resp && resp?.respcode === "200") {
            if (resp?.vcipstatus === StatusCodes.VCIPSTATUS_PENDING || resp?.vcipstatus === StatusCodes.VCIPSTATUS_REJECTED) {
                toast.success(resp?.respdesc);
                // sessionStorage.setItem('user', resp);
                // sessionStorage.setItem('authkey', resp?.authkey);
                // sessionStorage.setItem('vcipkey', resp?.vcipkey);
                yield put(actionGetVcipDetailsResponse(resp));
                if (action?.payload?.callback) {
                    action?.payload?.callback(resp, action?.payload?.reportFlag);
                }
            } else {
                toast.success(resp?.respdesc);
            }
        } else {
            toast.error(resp?.respdesc);
        }
    } catch (err) {
        if (err.response) {
            toast.error(err?.response?.data?.errors?.length && err?.response?.data?.errors[0]?.message);
        } else {
            toast.error(err.message);
        }
    } finally {
        yield put(actionReqResStatusLoaderSagaAction(false));
    }
}


// CREATE SLOT
const createSlotReq = (body) => {
    const URL = 'CreateSlot';
    return Axios.post(URL, body).then(res => { return res?.data })
}

function* createSlotReqSaga(action) {
    yield put(actionReqResStatusLoaderSagaAction(true));
    try {
        const resp = yield call(createSlotReq, action?.payload?.model);
        if (resp && resp?.respcode === "200") {
            toast.success(resp?.respdesc);
            if (action?.payload?.callback) {
                action?.payload?.callback(resp);
            }
        } else {
            toast.error(resp?.respdesc);
        }
    } catch (err) {
        if (err.response) {
            toast.error(err?.response?.data?.errors?.length && err?.response?.data?.errors[0]?.message);
        } else {
            toast.error(err.message);
        }
    } finally {
        yield put(actionReqResStatusLoaderSagaAction(false));
    }
}

export default function* getVcipDetailsWatcherSaga() {
    yield takeLatest(ACTION_GET_CUSTOMER_VCIP_DETAILS_REQ, getVcipDetialsReqSaga);
    yield takeLatest(ACTION_GET_CUSTOMER_CREATE_SLOT_REQ, createSlotReqSaga);
}