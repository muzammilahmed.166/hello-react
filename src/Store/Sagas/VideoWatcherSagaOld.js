import toast from 'react-hot-toast';
import { put, takeLatest, call, takeEvery } from 'redux-saga/effects';
import Axios from 'axios';
import base64 from 'base-64';
import AES256 from 'aes-everywhere';
import {
    ACTION_CREATE_VIDEO_SESSION_REQ, ACTION_CREATE_VIDEO_TOKEN_RECORD_REQ, ACTION_CREATE_VIDEO_TOKEN_REQ, ACTION_END_VIDEO_REQ
} from '../SagaActions/SagaActionTypes';
import {
    createVideoTokenForRecordingSagaAction,
    createVideoTokenSagaAction,
    createVideoTokenSagaActionResponse,
    recordingSessionStoreSagaAction
} from '../SagaActions/VideoSagaActions';
import { OpenVidu } from 'openvidu-browser';
import AxiosService from '../../Service/axios';

let KEY = 'f379e0b661ae4650b19169e4d93665dc';

const aesEncrypt = (data) => {
    var passphrase = KEY;
    let val1 = passphrase.substr(0, 4);
    let val2 = passphrase.substr(passphrase.length, 4);
    let updatedValue = val1 + passphrase + val2;
    const finalvalue = base64.encode(updatedValue).substr(0, 32);
    const encrypted = AES256.encrypt(JSON.stringify(data), finalvalue);
    return encrypted;
}

const aesDecrypt = (data) => {
    var passphrase = KEY;
    let val1 = passphrase.substr(0, 4);
    let val2 = passphrase.substr(passphrase.length, 4);
    let updatedValue = val1 + passphrase + val2;
    const finalvalue = base64?.encode(updatedValue).substr(0, 32);
    const decrypted = AES256?.decrypt(data, finalvalue);
    return decrypted;
}

const instance = Axios.create({
    // baseURL: OPENVIDU_SERVER_URL,
    baseURL: 'https://vcip.syntizen.com:3002/vkycrestservices/',
});

// CREATE SESSION
const createVideoSessionReq = (model) => {
    const URL = 'sessions';
    const data = {
        "recordingMode": "ALWAYS",
        "customSessionId": model?.sessionId,
        "defaultRecordingLayout": "CUSTOM",
    };
    const headers = {
        headers: {
            'Content-Type': 'application/json',
        }
    };
    const body = {
        data: aesEncrypt(data)
    }
    return instance.post(URL, body, headers).then(res => { return res?.data })
}

function* createVideoSessionReqSaga(action) {
    try {
        const resp = yield call(createVideoSessionReq, action?.payload?.model);
        var response = aesDecrypt(resp.data);
        yield put(createVideoTokenSagaAction(action?.payload));
        yield put(createVideoTokenForRecordingSagaAction(action?.payload));
    } catch (err) {
        var error = Object.assign({}, aesDecrypt(err?.data));
        if (error.response && error.response.status === 409) {
            // dispatch({
            //     type: actionTypes.CHAT_SESSION_ID,
            //     payload: sessionId
            // });
            yield put(createVideoTokenSagaAction(action?.payload));
            yield put(createVideoTokenForRecordingSagaAction(action?.payload));
        } else {
            console.warn(
                'No connection to OpenVidu Server. This may be a certificate error at ' + this.OPENVIDU_SERVER_URL,
            );
            if (
                window.confirm(
                    'No connection to OpenVidu Server. This may be a certificate error at "' +
                    this.OPENVIDU_SERVER_URL +
                    '"\n\nClick OK to navigate and accept it. ' +
                    'If no certificate warning is shown, then check that your OpenVidu Server is up and running at "' +
                    this.OPENVIDU_SERVER_URL +
                    '"',
                )
            ) {
                window.location.assign(this.OPENVIDU_SERVER_URL + '/accept-certificate');
            }
        }
    } finally {
        // yield put(actionReqResStatusLoaderSagaAction(false));
    }
}

// CREATE TOKEN
const createVideoTokenReq = (model) => {
    const URL = 'tokens';
    const data = {
        session: model?.sessionId
    }
    let body = {
        data: aesEncrypt(data)
    }
    const headers = {
        headers: {
            'Content-Type': 'application/json',
        }
    };
    return instance.post(URL, body, headers).then(res => { return res?.data })
}


function* createVideoTokenReqSaga(action) {
    try {
        let respData = yield call(createVideoTokenReq, action?.payload?.model);
        let response = JSON.parse(aesDecrypt(respData.data));
        let mySession = action?.payload?.model?.session;

        sessionStorage.setItem("session", response.session);
        // console.log(response);

        mySession.connect(response.token, "agent")
            .then(() => {
                let publisher =action?.payload?.model?.OV.initPublisher(undefined, {
                    audioSource: undefined,
                    videoSource: undefined,
                    publishAudio: true,
                    publishVideo: true,
                    // resolution: '640x480',
                    frameRate: 30,
                    insertMode: 'APPEND',
                    mirror: false,
                });
                sessionStorage.setItem("publisher", Object.keys(publisher));
                mySession.publish(publisher);
                action?.payload?.model?.setMainStreamManager(publisher);
                action?.payload?.model?.setPublisher(publisher);
                // $this.setState({
                //     mainStreamManager: publisher,
                //     publisher: publisher,
                // });
            })
            .catch((error) => {
                console.log('There was an error connecting to the session:', error.code, error.message);
            });
    } catch (err) {
        if (err.response) {
            toast.error(err?.response?.data?.errors?.length && err?.response?.data?.errors[0]?.message);
        } else {
            toast.error(err.message);
        }
    } finally {
        // yield put(actionReqResStatusLoaderSagaAction(false));
    }
}

function* createVideoTokenRecordReqSaga(action) {
    // console.log("action->->",action.payload);
    try {
        let respData = yield call(createVideoTokenReq, action?.payload?.model);
        let response = JSON.parse(aesDecrypt(respData.data));
        sessionStorage.setItem("session", response.session);

        const mySession = action?.payload?.model?.session;
        // var response = JSON.parse(aesDecrypt(resp.data.data));
        sessionStorage.setItem("session", response.session)
        let OV2 = new OpenVidu();
        let mySession2 = OV2.initSession();
        mySession2.connect(response.token, "screen")
            .then(() => {
                let publisher2 = OV2.initPublisher(undefined, {
                    audioSource: undefined,
                    videoSource: 'screen',
                    publishAudio: true,
                    publishVideo: true,
                    // resolution: '640x480',
                    frameRate: 30,
                    insertMode: 'APPEND',
                    mirror: false,
                });
                // sessionStorage.setItem("publisher", Object.keys(publisher));
                mySession2.publish(publisher2);
                action?.payload?.model?.setSession2(mySession2);
                // $this.setState({
                //     session2: mySession2
                // });
            })
            .catch((error) => {
                console.log('There was an error connecting to the session:', error.code, error.message);
            });
    } catch (err) {
        if (err.response) {
            toast.error(err?.response?.data?.errors?.length && err?.response?.data?.errors[0]?.message);
        } else {
            toast.error(err.message);
        }
    } finally {
        // yield put(actionReqResStatusLoaderSagaAction(false));
    }
}



export default function* VideoWatcherSaga() {
    yield takeEvery(ACTION_CREATE_VIDEO_SESSION_REQ, createVideoSessionReqSaga);
    yield takeEvery(ACTION_CREATE_VIDEO_TOKEN_REQ, createVideoTokenReqSaga);
    yield takeEvery(ACTION_CREATE_VIDEO_TOKEN_RECORD_REQ, createVideoTokenRecordReqSaga);
}