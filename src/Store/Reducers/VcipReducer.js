import * as SagaActionTypes from '../SagaActions/SagaActionTypes';
import * as actionTypes from '../SagaActions/actionTypes'

const initial = {
    vcipDetails: {},
    videoSessionData: {},
    aadhaarOfflineMatchData: {},
    videoTokenData: {},
}

const VcipReducer = (state = initial, action) => {
    switch (action.type) {
        case SagaActionTypes.ACTION_GET_CUSTOMER_VCIP_DETAILS_RES:
            return { ...state, vcipDetails: action.payload }

        case SagaActionTypes.ACTION_CREATE_VIDEO_TOKEN_RES:
            return { ...state, videoSessionData: action.payload }

        case actionTypes.ACTION_KYC_AADHAR_OFFLINE_RESPONSE:
            return { ...state, aadhaarOfflineMatchData: action.payload }

        case SagaActionTypes.ACTION_CREATE_VIDEO_TOKEN_RECORD_REQ:
            return { ...state, videoTokenData: action.payload }

        default:
            return state;
    }
}

export default VcipReducer;