import {
    ACTION_GET_CALL_HISTORY_REQ,
    ACTION_GET_DASHBOARD_INFO_REQ,
    ACTION_GET_DASHBOARD_LIVE_REQ,
    ACTION_GET_DASHBOARD_SCHEDULE_REQ,
} from "./actionTypes"

// LOGIN
export const getDashboardDataSagaAction = (payload) => {
    return {
        type: ACTION_GET_DASHBOARD_INFO_REQ,
        payload: payload
    }
}

// GET LIVE
export const getDashboardLiveSagaAction = (payload) => {
    return {
        type: ACTION_GET_DASHBOARD_LIVE_REQ,
        payload: payload
    }
}

// GET SCHEDULE
export const getDashboardScheduleSagaAction = (payload) => {
    return {
        type: ACTION_GET_DASHBOARD_SCHEDULE_REQ,
        payload: payload
    }
}

// GET AGENT CALL HISTORY
export const getCallHistorySagaAction = (payload) => {
    return {
        type: ACTION_GET_CALL_HISTORY_REQ,
        payload: payload
    }
}