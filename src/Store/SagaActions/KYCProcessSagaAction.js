import {
    ACTION_KYC_AADHAR_OFFLINE,
    ACTION_KYC_AADHAR_OFFLINE_RESPONSE,
    ACTION_KYC_FACEMATCH_CHECK_REQ,
    ACTION_KYC_GET_QUESTIONS,
    ACTION_KYC_MATCH_AADHAAR_PAN_CHECK_REQ,
    ACTION_KYC_SUBMIT_QUESTION
} from "./actionTypes"
import { ACTION_POST_UPLOAD_SCREEN_RECORDREQ } from "./SagaActionTypes"

export const getQuestionsSagaAction = (payload) => {
    return {
        type: ACTION_KYC_GET_QUESTIONS,
        payload: payload
    }
}

export const SubmitQuestionSagaAction = (payload) => {
    return {
        type: ACTION_KYC_SUBMIT_QUESTION,
        payload: payload
    }
}

export const livenessCheckSagaAction = (payload) => {
    return {
        type: ACTION_KYC_AADHAR_OFFLINE,
        payload: payload
    }
}

export const aadharOfflineKycResponseSagaAction = (payload) => {
    return {
        type: ACTION_KYC_AADHAR_OFFLINE_RESPONSE,
        payload: payload
    }
}

export const updateMatchStatusByAgentSagaAction = (payload) => {
    return {
        type: ACTION_KYC_MATCH_AADHAAR_PAN_CHECK_REQ,
        payload: payload
    }
}

export const faceMatchSagaAction = (payload) => {
    return {
        type: ACTION_KYC_FACEMATCH_CHECK_REQ,
        payload: payload
    }
}
export const uploadScreenRecordVideoSagaAction = (payload) => {
    return {
        type: ACTION_POST_UPLOAD_SCREEN_RECORDREQ,
        payload: payload
    }
}