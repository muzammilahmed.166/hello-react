import { ACTION_GET_DIGILOCKER_LINK_REQ, ACTION_GET_DIGILOCKER_STATUS_REQ } from "./SagaActionTypes";


export const actionAadharDigiLocker = (payload) => {
    return {
        type: ACTION_GET_DIGILOCKER_LINK_REQ,
        payload: payload
    }
}


export const actionAadharDigiLockerStatus = (payload) => {
    return {
        type: ACTION_GET_DIGILOCKER_STATUS_REQ,
        payload: payload
    }
}